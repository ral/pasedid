Parse and Assemble EDID
=======================

Parse and assemble EDID binary data blobs.

Usage
-----

Parse a binary EDID data blob into a yaml file:

    ./edidparse < edid.bin > edid.yml

**Note:** The `edidparse` tool expects the data in *binary* form.
Files in *hex* form need to be converted first, for example with:

    xxd -r -p edid.hex > edid.bin


Assemble an EDID data blob based on specifications read from a yaml file:

    ./edidassemble < edid.yml > edid.bin


**Note:** The `edidassemble` tool writes binary data to stdout which
is potentially dangerous. Redirect the output into a file or pipe.


For further usage information, run the comands with `--help`.

Notes
-----

Validate the EDID binary blob with the `edid-decode` tool:

    edid-decode -c < edid.bin

Links
-----

* https://git.linuxtv.org/edid-decode.git/about/
