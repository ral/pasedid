/// The configuration for parser and assembler behaviour.
#[derive(Debug, Clone, Copy)]
pub struct Config {
    /// Whether to handle extensions.
    pub handle_extensions: bool,
}

impl Default for Config {
    #[inline]
    fn default() -> Config {
        Config {
            handle_extensions: false,
        }
    }
}
