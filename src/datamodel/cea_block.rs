use serde::{Deserialize, Serialize};

/// The block tag of the CEA-861 extension block.
pub const BLOCK_TAG: u8 = 0x02;

// All together

/// Data model for the CEA-861 extension block.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct EDIDExtensionBlockCEA {
    /// Version number.
    pub version: u8,


    // TODO: Model CEA internals


    // Checksum.
    //pub checksum: u8,
}
