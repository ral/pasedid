use serde::{Deserialize, Serialize};

// All together

/// Data model for the DISCARD extension block.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct EDIDExtensionBlockDiscard {}
