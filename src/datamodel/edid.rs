use serde::{Deserialize, Serialize};

use super::base_block::EDIDBaseBlock;
use super::cea_block::EDIDExtensionBlockCEA;

/// Main EDID data model.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EDID {
    /// The base block of an EDID.
    pub base_block: EDIDBaseBlock,

    /// A list of extension blocks.
    pub extension_blocks: Vec<EDIDExtensionBlock>,
    // A list of (up to two) block maps.
    // block_maps: Option<EDIDMapBlock>
    // TODO: Just use two options?
}

/// EDID Extension block variants.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum EDIDExtensionBlock {
    /// CEA-861 extension block.
    CEA(EDIDExtensionBlockCEA),
    // TODO: Process more extension blocks
    // Video timing extension block.
    // VTB(EDIDExtensionBlockVTB),
    // Display information extension block.
    // DI(EDIDExtensionBlockDI),
    // Localized string extension block.
    // LS(EDIDExtensionBlockLS),
    // Digital packet video link extension block.
    // DPVL(EDIDExtensionBlockDPVL),
    // ...
    /// Discard extension block.
    DISCARD,
}

/// EDID extension block tags.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum ExtensionBlockTag {
    /// CEA-861 series extension.
    /// Block tag: `0x02`
    CEA,
    /// Video timing block extension.
    /// Block tag: `0x10`
    VTB,
    /// Display information extension.
    /// Block tag: `0x40`
    DI,
    /// Localized string extension.
    /// Block tag: `0x50`
    LS,
    /// Digital packet video link extension.
    /// Block tag: `0x60`
    DPVL,
    /// Extension block map.
    /// Block tag: `0xf0`
    BLOCKMAP,
    /// Defined by the display manufacturer.
    /// Block tag: `0xff`
    PROPRIETARY,
    /// Unknown extension block.
    /// Block tag: `_`
    UNKNOWN,
    /// Discard extension block
    // Fake block to discard a BLOCKSIZE worth of data
    DISCARD,
}
