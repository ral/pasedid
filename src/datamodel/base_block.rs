use serde::{Deserialize, Serialize};

/// EDID magic header bytes.
pub const MAGIC: [u8; 8] = [0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00];

/// Empty struct representing the EDID magic header.
#[derive(Debug, Clone, Copy, Default, Serialize, Deserialize)]
pub struct Header {}

/// The EDID structure version and revision.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Version {
    /// EDID standard version.
    pub version: u8,
    /// EDID standard revision.
    pub revision: u8,
}

// SECTION 3.4 Vendor and product

/// Manufacturer name.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct ManufacturerID(pub char, pub char, pub char);

/// Week and year of manufacture.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct ManufactureWeekYear {
    /// Week of manufacture.
    pub week: u8,
    /// Year of manufacture.
    pub year: u16,
}

/// Week and year of manufacture or model year.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum ManufactureDate {
    /// Week and year of manufacture.
    ManufactureWeekYear(ManufactureWeekYear),
    /// Model year.
    ManufactureModelYear(u16),
}

/// Vendor and product identification.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct VendorProductIdentification {
    /// Manufacturer name.
    pub manufacturer_id: ManufacturerID,
    /// Product code.
    pub product_code: u16,
    /// Serial number.
    pub serial_number: u32,
    /// Week and year of manufacture or model year.
    pub manufacture_date: ManufactureDate,
}

// SECTION 3.6 Basic display parameters and features

/// Signal voltage level in V peak to peak.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct SignalLevel {
    /// Video signal.
    pub video: f32,
    /// Sync signal.
    pub sync: f32,
}

/// Synchronization types.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct SynchronizationSupport {
    /// If true: separate sync H and V signals are supported.
    pub separate_sync: bool,
    /// If true: composite sync signal on horizontal is supported.
    pub composite_sync: bool,
    /// If true: composite sync signal on green video is supported.
    pub sync_on_green: bool,
}

/// Color bit depth.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum ColorBitDepth {
    /// Color bit depth is undefined.
    Undefined,
    /// 6 bits per primary color.
    PrimaryColorBits6,
    /// 8 bits per primary color.
    PrimaryColorBits8,
    /// 10 bits per primary color.
    PrimaryColorBits10,
    /// 12 bits per primary color.
    PrimaryColorBits12,
    /// 14 bits per primary color.
    PrimaryColorBits14,
    /// 16 bits per primary color.
    PrimaryColorBits16,
}

/// Digital video interface standard supported.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum DigitalVideoInterfaceStandard {
    /// Digital interface is not defined.
    Undefined,
    /// DVI is supported.
    DVI,
    /// HDMI-A is supported.
    HDMIA,
    /// HDMI-B is supported.
    HDMIB,
    /// MDDI is supported.
    MDDI,
    /// DisplayPort is supported.
    DP,
}

/// Video Input Definition.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum VideoInputDefinition {
    /// Analog video signal interface.
    Analog {
        /// Signal level standard.
        signal_level: SignalLevel,
        /// Video setup.
        /// If true: blank-to-black setup or pedestal expected.
        video_setup: bool,
        /// Synchronization Types.
        sync: SynchronizationSupport,
        /// Serrations
        /// If true: serration on the vertical sync is supported.
        serration: bool,
    },
    /// Digital video signal interface.
    Digital {
        /// Color bit depth.
        color_depth: ColorBitDepth,
        /// Digital video interface standard supported.
        video_interface: DigitalVideoInterfaceStandard,
    },
}

/// Horizontal and vertical screen size.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct ScreenSize {
    /// Horizontal screen size in centimeters.
    pub horizontal: u8,
    /// Vertical screen size in centimeters.
    pub vertical: u8,
}

/// Physical screen orientation.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum ScreenOrientation {
    /// Landscape screen orientation.
    LANDSCAPE,
    /// Portrait screen orientation.
    PORTRAIT,
}

/// Numerical aspect ratio.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct NumericAspectRatio {
    /// Screen orientation.
    pub orientation: ScreenOrientation,
    /// Numeric aspect ratio.
    pub ratio: f32,
}

/// Screen size or aspect ratio.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum ScreenSizeOrAspectRatio {
    /// Horizontal and vertical screen size.
    ScreenSize(ScreenSize),
    /// Aspect ratio.
    AspectRatio(NumericAspectRatio),
    /// Unspecified.
    Unknown,
}

/// Display transfer characteristics.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum TransferCharacteristic {
    /// Display transfer characteristic: gamma value.
    Gamma(f32),
    /// Gamma value is not defined here.
    SeeOther,
}

/// Display power management.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct DisplayPowerManagement {
    /// If true: standby mode is supported.
    pub standby_supported: bool,
    /// If true: suspend mode is supported.
    pub suspend_supported: bool,
    /// If true: active off (very low power mode) is supported.
    pub active_off_supported: bool,
}

/// Display color type.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum DisplayColorType {
    /// Monochrome or grayscale display.
    Grayscale,
    /// RGB color display.
    RGBColor,
    /// Non-RGB color display.
    OtherColor,
    /// Display color type is undefined.
    Undefined,
}

/// Supported color encoding formats.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum ColorEncodingFormat {
    /// RGB 4:4:4
    RGB444,
    /// RGB 4:4:4 and YCrCb 4:4:4
    RGB444YCrCb444,
    /// RGB 4:4:4 and YCrCb 4:2:2
    RGB444YCrCb422,
    /// RGB 4:4:4 and YCrCb 4:4:4 and YCrCb 4:2:2
    RGB444YCrCb444YCrCb422,
}

/// Display color type or color encoding format.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum DisplayColorTypeOrColorEncodingFormat {
    /// Display color type.
    DisplayColorType(DisplayColorType),
    /// Color encoding format.
    ColorEncodingFormat(ColorEncodingFormat),
}

/// Feature support flags.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct FeatureSupportFlags {
    /// If true: sRGB standard is the default color space.
    pub srgb_default: bool,
    /// If true: preferred timing mode includes the native pixel format
    /// and preferred refresh rate of the display device.
    pub preferred_timing_mode: bool,
    /// If true: display is continuous frequency.
    pub continuous_frequency: bool,
}

/// Display feature support.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct DisplayFeatures {
    /// Display power management.
    pub dpm_support: DisplayPowerManagement,
    /// Display color type or supported color encoding formats.
    pub color_support: DisplayColorTypeOrColorEncodingFormat,
    /// Other feature support flags.
    pub feature_support_flags: FeatureSupportFlags,
}

/// Basic display parameters and features.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct DisplayParameters {
    /// Video input definition.
    pub video_input_definition: VideoInputDefinition,
    /// Horizontal and vertical screen size or aspect ratio.
    pub screen_size: ScreenSizeOrAspectRatio,
    /// Display transfer characteristics.
    pub gamma: TransferCharacteristic,
    /// Feature support.
    pub features: DisplayFeatures,
}

// SECTION 3.7 Display chromaticity coordinates

/// Pair of x,y coordinates in the CIE 1931 space.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct CIECoordinates {
    /// The x chromaticity coordinate.
    pub x: f32,
    /// The y chromaticity coordinate.
    pub y: f32,
}

/// Display x,y chromaticity coordinates.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct ChromaticityCoordinates {
    /// Red component.
    pub red: CIECoordinates,
    /// Green component.
    pub green: CIECoordinates,
    /// Blue component.
    pub blue: CIECoordinates,
    /// White point.
    pub white: CIECoordinates,
}

// SECTION 3.8 Established timings I and II

/// Established Timings I and II.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum EstablishedTiming {
    /// This represents a display setting of 720 × 400 at 70Hz.
    H720V400F70,
    /// This represents a display setting of 720 × 400 at 88Hz.
    H720V400F88,
    /// This represents a display setting of 640 × 480 at 60Hz.
    H640V480F60,
    /// This represents a display setting of 640 × 480 at 67Hz.
    H640V480F67,
    /// This represents a display setting of 640 × 480 at 72Hz.
    H640V480F72,
    /// This represents a display setting of 640 × 480 at 75Hz.
    H640V480F75,
    /// This represents a display setting of 800 × 600 at 56Hz.
    H800V600F56,
    /// This represents a display setting of 800 × 600 at 60Hz.
    H800V600F60,

    /// This represents a display setting of 800 × 600 at 72Hz.
    H800V600F72,
    /// This represents a display setting of 800 × 600 at 75Hz.
    H800V600F75,
    /// This represents a display setting of 832 × 624 at 75Hz.
    H832V624F75,
    /// This represents a display setting of 1024 × 768 at 87Hz interlaced.
    H1024V768F87,
    /// This represents a display setting of 1024 × 768 at 60Hz.
    H1024V768F60,
    /// This represents a display setting of 1024 × 768 at 70Hz.
    H1024V768F70,
    /// This represents a display setting of 1024 × 768 at 75Hz.
    H1024V768F75,
    /// This represents a display setting of 1280 × 1024 at 75Hz.
    H1280V1024F75,

    /// This represents a display setting of 1152 × 870 at 75Hz.
    H1152V870F75,
}

/// List of supported established timings.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EstablishedTimings {
    /// List of supported established timings.
    pub established_timings: Vec<EstablishedTiming>,
    // Manufacturer specified timings.
    //pub manufacturer_timings: Vec<ManufacturerTiming>,
}

// SECTION 3.9 Standard timings

/// Symbolic image aspect ratio constants.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum AspectRatioSymbol {
    // Aspect ratio of 1 : 1.
    // AR1TO1,
    /// Aspect ratio of 4 : 3.
    AR4TO3,
    /// Aspect ratio of 5 : 4.
    AR5TO4,
    /// Aspect ratio of 16 : 9.
    AR16TO9,
    /// Aspect ratio of 16 : 10.
    AR16TO10,
}

/// Standard Timing.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct StandardTiming {
    /// Number of horizontal addressable pixels.
    /// The range is 256 to 2288 pixels, in increments of 8 pixels.
    /// Raw value is encoded as `horizontal_resolution / 8 - 31`.
    pub horizontal_resolution: u16,
    /// Image aspect ratio.
    pub aspect_ratio: AspectRatioSymbol,
    /// Field refresh rate.
    /// The field refresh rate in Hz.
    /// The range is 60 to 123 Hz.
    /// Raw value is encoded as `refresh_rate - 60`.
    pub refresh_rate: u8,
}

/// Standard Timings.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct StandardTimings(pub Vec<StandardTiming>);

// SECTION 3.10 18 Byte descriptors

// SECTION 3.10.2 Detailed timing descriptor

/// Addressable video.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct AddressableVideo {
    /// Horizontal addressable video in pixels.
    pub horizontal: u16,
    /// Vertical addressable video in lines.
    pub vertical: u16,
}

/// Blanking.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Blanking {
    /// Horizontal blanking in pixels.
    pub horizontal: u16,
    /// Vertical blanking in lines.
    pub vertical: u16,
}

/// Front porch.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct FrontPorch {
    /// Horizontal front porch in pixels.
    pub horizontal: u16,
    /// Vertical front porch in lines.
    pub vertical: u8,
}

/// Sync pulse width.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct SyncPulseWidth {
    /// Horizontal sync pulse width in pixels.
    pub horizontal: u16,
    /// Vertical sync pulse width in lines.
    pub vertical: u8,
}

/// Video image size.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct VideoImageSize {
    /// Horizontal addressable video image size in mm.
    pub width: u16,
    /// Vertical addressable video image size in mm.
    pub height: u16,
}

/// Border definition.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct BorderSize {
    /// Top vertical border in lines.
    /// Equals bottom vertical border.
    pub top: u8,
    /// Left horizontal border in pixels.
    /// Equals right horizontal border.
    pub left: u8,
}

/// Signal interface type.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum SignalInterface {
    /// Progressive signal interface.
    NonInterlaced,
    /// Interlaced signal interface.
    Interlaced,
}

/// Stereo viewing support.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum StereoViewingType {
    /// Do not care
    DoNotCare,
    /// Normal display, no stereo.
    NoStereo,
    /// Field sequential stereo, right image when stereo sync signal is 1.
    SequentialRightSync,
    /// Field sequential stereo, left image when stereo sync signal is 1.
    SequentialLeftSync,
    /// Two-way interleaved stereo, right image on even lines.
    InterleavedLinesRightEven,
    /// Two-way interleaved stereo, left image on even lines.
    InterleavedLinesLeftEven,
    /// Four-way interleaved stereo.
    Interleaved4Way,
    /// Side-by-side interleaved stereo.
    SideBySide,
}

/// Analog sync signal type.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum SyncSignal {
    /// Analog composite (horizontal and vertical combined) sync signal.
    AnalogComposite,
    /// Bipolar analog composite (horizontal and vertical combined) sync signal.
    BipolarAnalogComposite,
}

/// A line to perform sync on.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum SyncLine {
    /// Sync pulse signal is on all three lines.
    RGB,
    /// Sync pulse signal is on green line only.
    Green,
}

/// The direction (polarity) of the sync pulse.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum SyncPolarity {
    /// Sync pulse polarity is positive.
    Positive,
    /// Sync pulse polarity is negative.
    Negative,
}

/// Sync Signal Definitions.
// TODO: Recheck if this data model is actually correct.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum SyncSignalDefinition {
    /// Analog signal with combined sync information.
    AnalogComposite {
        /// Sync signals.
        sync: SyncSignal,
        /// HSync during VSync.
        serrations: bool,
        /// Which signal line to sync on.
        line: SyncLine,
    },
    /// Digital signal with combined sync information.
    DigitalComposite {
        /// HSync during VSync.
        serrations: bool,
        /// Horizontal sync signal polarity.
        horizontal: SyncPolarity,
    },
    /// Digital signal with separate sync information.
    DigitalSeparate {
        /// Horizontal sync signal polarity.
        horizontal: SyncPolarity,
        /// Vertical sync signal polarity.
        vertical: SyncPolarity,
    },
}

/// Detailed timing descriptor.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct DetailedTiming {
    /// The pixel clock in Hz.
    /// The range is 10 kHz to 655.35 MHz in 10 kHz steps.
    /// Raw value is encoded as `pixel_clock / 10_000`.
    pub pixel_clock: u32,
    /// Addressable video in pixels and lines.
    pub addressable_video: AddressableVideo,
    /// Blanking in pixels and lines.
    pub blanking: Blanking,
    /// Front porch in pixels and lines.
    pub front_porch: FrontPorch,
    /// Sync pulse width in pixels and lines.
    pub sync_pulse_width: SyncPulseWidth,
    /// Video image size in milimeters.
    pub video_image_size: VideoImageSize,
    /// Border size in pixels and lines.
    pub border_size: BorderSize,
    /// Signal interface type.
    pub signal_interface: SignalInterface,
    /// Stereo viewing support.
    pub stereo_viewing: StereoViewingType,
    /// Sync signal definitions.
    pub sync_signal_type: SyncSignalDefinition,
}

/// Detailed timing descriptors.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DetailedTimings(pub Vec<DetailedTiming>);

// SECTION 3.10.3 Display descriptor definitions

/// Display product serial number descriptor.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ProductSerialNumber {
    /// Serial number.
    pub serial_number: String,
}

/// Alphanumeric data string descriptor.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AlphanumericDataString {
    /// Alphanumeric (ASCII) data string.
    pub string: String,
}

/// Range limits offset.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum RangeLimitOffset {
    /// No offset.
    None,
    /// Only maximum is offset.
    Max,
    /// Minimum and maximum are offset.
    MinMax,
}

/// Range limits offsets.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct RangeLimitOffsets {
    /// Vertical offset.
    pub vertical: RangeLimitOffset,
    /// Horizontal offset.
    pub horizontal: RangeLimitOffset,
}

/// Minimum and maximum of range.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct MinMaxRange<T> {
    /// Minimum value.
    pub min: T,
    /// Maximum value.
    pub max: T,
}

/// Video timing support flags.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum TimingSupportFlags {
    /// Default GTF (Generalized Timing Formula) is supported.
    DefaultGTF,
    /// Range limits only.
    RangeLimitsOnly,
    /// Secondary GTF curve is supported.
    SecondaryGTF,
    /// CVT (Coordinated Video Timing) is supported.
    CVTSupport,
    // Reserved for future timing.
    // Reserved
}

/// Generalized timing formula (GTF) definition.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct GTFCurveDefinition {
    /// Horizontal break frequency for secondary curve in Hz.
    /// The raw value is encoded as `(break_frequency) / 2` in kHz.
    pub break_frequency: u32,
    /// Parameter `2*C`: valid range is `0 <= C <= 127`.
    pub c: u8,
    /// Parameter `M`: valid range is `0 <= M <= 65535`.
    pub m: u16,
    /// Parameter `K`: valid range is `0 <= K <= 255`.
    pub k: u8,
    /// Parameter `2*J`: valid range is `0 <= J <= 127`.
    pub j: u8,
}

/// Maximum active pixels per line
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum ActivePixels {
    /// Unlimited number of active pixels.
    Unlimited,
    /// Miximum number of active pixels.
    Maximum(u16),
}

/// Symbolic aspect ratios for use in CVT support definitions.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum CVTAspectRatio {
    /// Aspect ratio of `4 : 3`.
    AR4TO3,
    /// Aspect ratio of `16 : 9`.
    AR16TO9,
    /// Aspect ratio of `16 : 10`.
    AR16TO10,
    /// Aspect ratio of `5 : 4`.
    AR5TO4,
    /// Aspect ratio of `15 : 9`.
    AR15TO9,
}

/// The CVT blanking support type.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct CVTBlankingSupport {
    /// Standard blanking is supported.
    pub standard: bool,
    /// Reduced blanking is supported.
    pub reduced: bool,
}

/// The CVT display scaling type.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum CVTDisplayScaling {
    /// Horizontal shrink.
    HorizontalShrink,
    /// Horizontal stretch.
    HorizontalStretch,
    /// Vertical shrink.
    VerticalShrink,
    /// Vertical stretch.
    VerticalStretch,
    // Reserved
}

/// Display range limits with CVT support definition.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CVTDefinition {
    /// CVT standard version number.
    pub standard_version: (u8, u8),
    /// Additional (negative) pixel clock precision offset in Hz.
    pub pixel_clock_precision: u32,
    /// Maximum of active pixels per line.
    pub active_pixels_per_line: ActivePixels,
    /// List of supported aspect ratios.
    pub supported_aspect_ratios: Vec<CVTAspectRatio>,
    /// The preferred aspect ratio.
    pub preferred_aspect_ratio: CVTAspectRatio,
    /// CVT blanking support.
    pub blanking_support: CVTBlankingSupport,
    /// Display scaling type supported.
    pub display_scaling_type: Vec<CVTDisplayScaling>,
    /// Preferred vertical refresh rate in Hz.
    pub preferred_vertical_refresh_rate: u8,
}

/// Parameters for a secondary timing formula.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum VideoTimingData {
    /// Nothing, filled with padding.
    None,
    /// Display range limits with GTF secondary curve.
    GTF(GTFCurveDefinition),
    /// Display range limits with CVT support.
    CVT(CVTDefinition),
    // Unknown extension
    // Reserved(u8, [u8; 7]),
}

/// Display range limits.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RangeLimits {
    /// Display range limits offsets.
    pub range_limits_offsets: RangeLimitOffsets,
    /// Vertical frequency min and max limit in Hz, with offsets applied.
    pub vertical_rate: MinMaxRange<u32>,
    /// Horizontal frequency min and max limit in Hz, with offsets applied.
    pub horizontal_rate: MinMaxRange<u32>,
    /// Pixel frequency limit in Hz.
    pub max_pixel_clock: u32,
    /// Video timing support flags.
    pub timing_support_flags: TimingSupportFlags,
    /// Video timing data.
    pub video_timing_data: VideoTimingData,
}

/// Display product name string descriptor.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ProductName {
    /// Product name.
    pub name: String,
}

/// A single color point descriptor.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct WhitePointDatum {
    /// The white point index number.
    pub index: u8,
    /// The actual white point data.
    pub white_point: CIECoordinates,
    /// The associated gamma value.
    pub gamma: TransferCharacteristic,
}

/// Color point descriptor.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ColorPointData {
    /// Up to two additional white points with associated data.
    pub white_points: Vec<WhitePointDatum>,
}

/// Standard timing identifier descriptor.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct StandardTimingIdentifiers {
    /// Up to 6 standard timing identification codes.
    pub timings: Vec<StandardTiming>,
}

/// Color management data descriptor.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct ColorManagementData {
    /// Version number.
    pub version: u8,
    /// Red a3 and a2.
    pub red: (u16, u16),
    /// Green a3 and a2.
    pub green: (u16, u16),
    /// Blue a3 and a2.
    pub blue: (u16, u16),
}

/// Vertical refresh rates.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum VerticalRate {
    /// A vertical refresh rate of 50 Hz.
    RR50,
    /// A vertical refresh rate of 60 Hz.
    RR60,
    /// A vertical refresh rate of 75 Hz.
    RR75,
    /// A vertical refresh rate of 85 Hz.
    RR85,
}

/// Type of blanking.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum BlankingType {
    /// Standard blanking (CRT style).
    Standard,
    /// Reduced blanking (CVT Standard).
    Reduced,
}

/// Coordinated video timing 3-byte code.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CVTTimingCode {
    /// Number of addressable lines per field.
    /// Raw value is encoded as `lines_per_field / 2 - 1`.
    pub lines_per_field: u16,
    /// The aspect ratio.
    /// Note: not all values of the enum type are actually valid choices here.
    pub aspect_ratio: CVTAspectRatio,
    /// The preferred vertical refresh rate.
    pub preferred_vertical_rate: VerticalRate,
    /// The supported vertical refresh rates and blanking styles.
    pub supported_vertical_rates: Vec<(VerticalRate, BlankingType)>,
}

/// CVT 3-byte code descriptor.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CVTTimingCodes {
    /// Revision number.
    pub version: u8,
    /// List of up to 4 items of CVT timing code blocks.
    pub cvt_codes: Vec<CVTTimingCode>,
}

/// Established timings III definitions.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum EstablishedTimingIII {
    /// This represents a display setting of 640 × 350 at 85 Hz.
    H640V350F85,
    /// This represents a display setting of 640 × 400 at 85 Hz.
    H640V400F85,
    /// This represents a display setting of 720 × 400 at 85 Hz.
    H720V400F85,
    /// This represents a display setting of 640 × 480 at 85 Hz.
    H640V480F85,
    /// This represents a display setting of 848 × 480 at 60 Hz.
    H848V480F60,
    /// This represents a display setting of 800 × 600 at 85 Hz.
    H800V600F85,
    /// This represents a display setting of 1024 × 768 at 85 Hz.
    H1024V768F85,
    /// This represents a display setting of 1152 × 864 at 75 Hz.
    H1152V864F75,

    /// This represents a display setting of 1280 × 768 at 60 Hz with reduced blanking.
    H1280V768F60RB,
    /// This represents a display setting of 1280 × 768 at 60 Hz.
    H1280V768F60,
    /// This represents a display setting of 1280 × 768 at 75 Hz.
    H1280V768F75,
    /// This represents a display setting of 1280 × 768 at 85 Hz.
    H1280V768F85,
    /// This represents a display setting of 1280 × 960 at 60 Hz.
    H1280V960F60,
    /// This represents a display setting of 1280 × 960 at 85 Hz.
    H1280V960F85,
    /// This represents a display setting of 1280 × 1024 at 60 Hz.
    H1280V1024F60,
    /// This represents a display setting of 1280 × 1024 at 85 Hz.
    H1280V1024F85,

    /// This represents a display setting of 1360 × 768 at 60 Hz.
    H1360V768F60,
    /// This represents a display setting of 1440 × 900 at 60 Hz with reduced blanking.
    H1440V900F60RB,
    /// This represents a display setting of 1440 × 900 at 60 Hz.
    H1440V900F60,
    /// This represents a display setting of 1440 × 900 at 75 Hz.
    H1440V900F75,
    /// This represents a display setting of 1440 × 900 at 85 Hz.
    H1440V900F85,
    /// This represents a display setting of 1400 × 1050 at 60 Hz with reduced blanking.
    H1400V1050F60RB,
    /// This represents a display setting of 1400 × 1050 at 60 Hz.
    H1400V1050F60,
    /// This represents a display setting of 1400 × 1050 at 75 Hz.
    H1400V1050F75,

    /// This represents a display setting of 1400 × 1050 at 85 Hz.
    H1400V1050F85,
    /// This represents a display setting of 1680 × 1050 at 60 Hz with reduced blanking.
    H1680V1050F60RB,
    /// This represents a display setting of 1680 × 1050 at 60 Hz.
    H1680V1050F60,
    /// This represents a display setting of 1680 × 1050 at 75 Hz.
    H1680V1050F75,
    /// This represents a display setting of 1680 × 1050 at 85 Hz.
    H1680V1050F85,
    /// This represents a display setting of 1600 × 1200 at 60 Hz.
    H1600V1200F60,
    /// This represents a display setting of 1600 × 1200 at 65 Hz.
    H1600V1200F65,
    /// This represents a display setting of 1600 × 1200 at 70 Hz.
    H1600V1200F70,

    /// This represents a display setting of 1600 × 1200 at 75 Hz.
    H1600V1200F75,
    /// This represents a display setting of 1600 × 1200 at 85 Hz.
    H1600V1200F85,
    /// This represents a display setting of 1792 × 1344 at 60 Hz.
    H1792V1344F60,
    /// This represents a display setting of 1792 × 1344 at 75 Hz.
    H1792V1344F75,
    /// This represents a display setting of 1856 × 1392 at 60 Hz.
    H1856V1392F60,
    /// This represents a display setting of 1856 × 1392 at 75 Hz.
    H1856V1392F75,
    /// This represents a display setting of 1920 × 1200 at 60 Hz with reduced blanking.
    H1920V1200F60RB,
    /// This represents a display setting of 1920 × 1200 at 60 Hz.
    H1920V1200F60,

    /// This represents a display setting of 1920 × 1200 at 75 Hz.
    H1920V1200F75,
    /// This represents a display setting of 1920 × 1200 at 85 Hz.
    H1920V1200F85,
    /// This represents a display setting of 1920 × 1440 at 60 Hz.
    H1920V1440F60,
    /// This represents a display setting of 1920 × 1440 at 75 Hz.
    H1920V1440F75,
}

/// Established timings III descriptor.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EstablishedTimingsIII {
    /// Revision number.
    pub version: u8,
    /// List of established timings supported.
    pub timings: Vec<EstablishedTimingIII>,
}

/// Reserved data tag descriptor,
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct ReservedDescriptor {
    /// Descriptor tag.
    pub tag: u8,
    // Do not use.
    //pub bytes: [u8; 13],
}

/// Dummy descriptor.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct DummyDescriptor {}

/// Manufacturer specified data descriptor.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ManufacturerSpecified {
    /// Descriptor tag.
    pub tag: u8,
    /// Proprietary data bytes as hex string.
    //pub bytes: [u8; 13],
    pub bytes: String,
}

/// Display descriptor.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum DisplayDescriptor {
    /// Display product serial number.
    /// Descriptor tag: `0xff`.
    ProductSerialNumber(ProductSerialNumber),
    /// Alphanumeric data string.
    /// Descriptor tag: `0xfe`.
    AlphanumericDataString(AlphanumericDataString),
    /// Display range limits.
    /// Descriptor tag: `0xfd`.
    RangeLimits(RangeLimits),
    /// Display product name.
    /// Descriptor tag: `0xfc`.
    ProductName(ProductName),
    /// Color point data.
    /// Descriptor tag: `0xfb`.
    ColorPointData(ColorPointData),
    /// Standard timing identifications.
    /// Descriptor tag: `0xfa`.
    StandardTimingIdentifiers(StandardTimingIdentifiers),
    /// Display color management data.
    /// Descriptor tag: `0xf9`.
    ColorManagementData(ColorManagementData),
    /// CVT 3-byte timing codes.
    /// Descriptor tag: `0xf8`.
    CVTTimingCodes(CVTTimingCodes),
    /// Established timings III.
    /// Descriptor tag: `0xf7`.
    EstablishedTimingsIII(EstablishedTimingsIII),
    /// Reserved: currently undefined.
    /// Descriptor tags: `0xf6 .. 0x11`
    ReservedDescriptor(ReservedDescriptor),
    /// Dummy descriptor.
    /// Descriptor tag: `0x10`.
    DummyDescriptor(DummyDescriptor),
    /// Manufacturer specified display descriptor.
    /// Descriptor tags: `0x0f .. 0x00`
    ManufacturerSpecified(ManufacturerSpecified),
}

/// Display descriptors.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DisplayDescriptors(pub Vec<DisplayDescriptor>);

// SECTION 3.11 Extension Flag and Checksum

// No custom types here

// All together

/// Data model for the EDID base block.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EDIDBaseBlock {
    /// The EDID magic bytes header.
    #[serde(skip)]
    pub header: Header,

    /// The EDID structure version and revision.
    pub version: Version,

    /// Vendor and product identification.
    pub product: VendorProductIdentification,

    /// Basic display parameters and features.
    pub display_parameters: DisplayParameters,

    /// Display x,y chromaticity coordinates.
    pub chromaticity: ChromaticityCoordinates,

    /// Established timings.
    pub established_timings: EstablishedTimings,

    /// Standard timings.
    pub standard_timings: StandardTimings,

    /// Detailed timing descriptors.
    pub detailed_timings: DetailedTimings,

    /// Display descriptors.
    pub display_descriptors: DisplayDescriptors,

    /// Extension block count.
    pub extension_count: u8,

    /// Checksum.
    #[serde(skip)]
    pub checksum: u8,
}
