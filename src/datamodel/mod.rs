/// The complete EDID data model.
pub mod edid;

/// The data model for the EDID base block.
pub mod base_block;
/// The data model for the CEA-861 extension block.
pub mod cea_block;
/// The data model for the DISCARD extension block.
pub mod discard_block;
