#![allow(clippy::drop_non_drop)]
#![allow(clippy::enum_glob_use)]
#![allow(clippy::unusual_byte_groupings)]
#![allow(clippy::unreadable_literal)]
#![allow(clippy::wildcard_imports)]

use concat_arrays::concat_arrays;
use hex::decode;

use crate::assembler::types::{Assemble, AssemblyError};
use crate::checksum::construct_checksum;
use crate::datamodel::base_block::*;
use crate::definitions::*;

// SECTION 3.3 Header

impl Assemble for Header {
    type Data = [u8; 8];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        Ok(MAGIC)
    }
}

// SECTION 3.4 Vendor and product

impl Assemble for ManufacturerID {
    type Data = [u8; 2];

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let c0 = self.0 as u16;
        let c1 = self.1 as u16;
        let c2 = self.2 as u16;

        let b0 = (c0 << 10) & 0b0_11111_00000_00000;
        let b1 = (c1 <<  5) & 0b0_00000_11111_00000;
        let b2 = c2         & 0b0_00000_00000_11111;

        let mid = b0 | b1 | b2;
        Ok(mid.to_be_bytes())
    }
}

fn assemble_manufacture_year(year: u16) -> u8 {
    (year - 1990).to_le_bytes()[0]
}

impl Assemble for ManufactureWeekYear {
    type Data = [u8; 2];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let week = self.week;
        let year = assemble_manufacture_year(self.year);
        Ok([week, year])
    }
}

fn assemble_manufacture_model_year(year: u16) -> [u8; 2] {
    let tag = 0xff;
    let year = assemble_manufacture_year(year);
    [tag, year]
}

impl Assemble for ManufactureDate {
    type Data = [u8; 2];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use ManufactureDate::*;
        match self {
            ManufactureWeekYear(week_year) => week_year.assemble(),
            ManufactureModelYear(year) => Ok(assemble_manufacture_model_year(*year)),
        }
    }
}

impl Assemble for VendorProductIdentification {
    type Data = [u8; 10];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let manufacturer_id = self.manufacturer_id.assemble()?;
        let product_code = self.product_code.to_le_bytes();
        let serial_number = self.serial_number.to_le_bytes();
        let manufacture_date = self.manufacture_date.assemble()?;
        Ok(concat_arrays!(
            manufacturer_id,
            product_code,
            serial_number,
            manufacture_date
        ))
    }
}

// SECTION 3.5 Structure version and revision

impl Assemble for Version {
    type Data = [u8; 2];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        Ok([self.version, self.revision])
    }
}

// SECTION 3.6 Basic display parameters and features

impl Assemble for SignalLevel {
    type Data = u8;

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let eps = 0.0001;
        let bits = if (self.video - 0.700).abs() < eps && (self.sync - 0.300).abs() < eps {
            0u8 << 5
        } else if (self.video - 0.714).abs() < eps && (self.sync - 0.286).abs() < eps {
            1u8 << 5
        } else if (self.video - 1.000).abs() < eps && (self.sync - 0.400).abs() < eps {
            2u8 << 5
        } else if (self.video - 0.700).abs() < eps && (self.sync - 0.000).abs() < eps {
            3u8 << 5
        } else {
            unreachable!();
        };
        Ok(bits)
    }
}

impl Assemble for SynchronizationSupport {
    type Data = u8;

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let separate_sync = u8::from(self.separate_sync);
        let composite_sync = u8::from(self.composite_sync);
        let sync_on_green = u8::from(self.sync_on_green);
        let data = separate_sync << 3 | composite_sync << 2 | sync_on_green << 1;
        Ok(data)
    }
}

impl Assemble for ColorBitDepth {
    type Data = u8;

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use ColorBitDepth::*;
        let flags = match self {
            Undefined =>          0b000,
            PrimaryColorBits6 =>  0b001,
            PrimaryColorBits8 =>  0b010,
            PrimaryColorBits10 => 0b011,
            PrimaryColorBits12 => 0b100,
            PrimaryColorBits14 => 0b101,
            PrimaryColorBits16 => 0b110,
        };
        let data = flags << 4;
        Ok(data)
    }
}

impl Assemble for DigitalVideoInterfaceStandard {
    type Data = u8;

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use DigitalVideoInterfaceStandard::*;
        let flags = match self {
            Undefined => 0b0000,
            DVI =>       0b0001,
            HDMIA =>     0b0010,
            HDMIB =>     0b0011,
            MDDI =>      0b0100,
            DP =>        0b0101,
        };
        Ok(flags)
    }
}

impl Assemble for VideoInputDefinition {
    type Data = [u8; 1];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use VideoInputDefinition::*;
        let data = match self {
            Analog {
                signal_level,
                video_setup,
                sync,
                serration,
            } => {
                let intype = 0b0000_0000;
                let level = signal_level.assemble()?;
                let setup = u8::from(*video_setup) << 4;
                let synctype = sync.assemble()?;
                let ser = u8::from(*serration);
                intype | level | setup | synctype | ser
            }
            Digital {
                color_depth,
                video_interface,
            } => {
                let intype = 0b1000_0000;
                let cdepth = color_depth.assemble()?;
                let vinterface = video_interface.assemble()?;
                intype | cdepth | vinterface
            }
        };
        Ok([data])
    }
}

impl Assemble for ScreenSize {
    type Data = [u8; 2];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        Ok([self.horizontal, self.vertical])
    }
}

impl Assemble for NumericAspectRatio {
    type Data = [u8; 2];

    #[allow(clippy::cast_sign_loss)]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let data = match self.orientation {
            ScreenOrientation::LANDSCAPE => {
                let value = (100.0 * self.ratio) - 99.0;
                [value.round() as u8, 0x0]
            }
            ScreenOrientation::PORTRAIT => {
                let value = (100.0 / self.ratio) - 99.0;
                [0x0, value.round() as u8]
            }
        };
        Ok(data)
    }
}

impl Assemble for ScreenSizeOrAspectRatio {
    type Data = [u8; 2];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use ScreenSizeOrAspectRatio::*;
        let data = match self {
            ScreenSize(size) => size.assemble()?,
            AspectRatio(ar) => ar.assemble()?,
            Unknown => [0x00, 0x00],
        };
        Ok(data)
    }
}

impl Assemble for TransferCharacteristic {
    type Data = [u8; 1];

    #[allow(clippy::cast_sign_loss)]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use TransferCharacteristic::*;
        let data = match self {
            Gamma(value) => (value * 100.0 - 100.0).round() as u8,
            SeeOther => 0x00,
        };
        Ok([data])
    }
}

impl Assemble for DisplayPowerManagement {
    type Data = u8;

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let stb = u8::from(self.standby_supported) << 2;
        let sus = u8::from(self.suspend_supported) << 1;
        let aof = u8::from(self.active_off_supported);
        let data = (stb | sus | aof) << 5;
        Ok(data)
    }
}

impl Assemble for DisplayColorType {
    type Data = u8;

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use DisplayColorType::*;
        let flags = match self {
            Grayscale => 0b00,
            RGBColor => 0b01,
            OtherColor => 0b10,
            Undefined => 0b11,
        };
        let data = flags << 3;
        Ok(data)
    }
}

impl Assemble for ColorEncodingFormat {
    type Data = u8;

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use ColorEncodingFormat::*;
        let flags = match self {
            RGB444 => 0b00,
            RGB444YCrCb444 => 0b01,
            RGB444YCrCb422 => 0b10,
            RGB444YCrCb444YCrCb422 => 0b11,
        };
        let data = flags << 3;
        Ok(data)
    }
}

impl Assemble for FeatureSupportFlags {
    type Data = u8;

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let rgb = u8::from(self.srgb_default);
        let ptm = u8::from(self.preferred_timing_mode);
        let cfr = u8::from(self.continuous_frequency);
        let data = rgb << 2 | ptm << 1 | cfr;
        Ok(data)
    }
}

impl Assemble for DisplayFeatures {
    type Data = [u8; 1];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use DisplayColorTypeOrColorEncodingFormat::*;
        let dpm_support = self.dpm_support.assemble()?;
        let color_support = match self.color_support {
            DisplayColorType(x) => x.assemble()?,
            ColorEncodingFormat(x) => x.assemble()?,
        };
        let feature_support_flags = self.feature_support_flags.assemble()?;
        let data = dpm_support | color_support | feature_support_flags;
        Ok([data])
    }
}

impl Assemble for DisplayParameters {
    type Data = [u8; 5];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let video_input_definition = self.video_input_definition.assemble()?;
        let screen_size = self.screen_size.assemble()?;
        let gamma = self.gamma.assemble()?;
        let features = self.features.assemble()?;
        Ok(concat_arrays!(
            video_input_definition,
            screen_size,
            gamma,
            features
        ))
    }
}

// SECTION 3.7 Display chromaticity coordinates

impl Assemble for CIECoordinates {
    type Data = ((u8, u8), (u8, u8));

    #[allow(clippy::cast_sign_loss)]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Convert a float into a 10 bit binary fraction representation
        let uint = |value: f32| (value * 1024.0).round() as u16;

        // Split a 10 bit number into 8 high and 2 low bits
        let split = |value: u16| ((value >> 2 & 0xff) as u8, (value & 0b11) as u8);

        // Encode CIE coordinates into components
        let encode = |value: CIECoordinates| (split(uint(value.x)), split(uint(value.y)));

        // Encode components
        let data = encode(*self);
        Ok(data)
    }
}

impl Assemble for ChromaticityCoordinates {
    type Data = [u8; 10];

    #[allow(clippy::similar_names)]
    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Extract all components
        let ((rx_h, rx_l), (ry_h, ry_l)) = self.red.assemble()?;
        let ((gx_h, gx_l), (gy_h, gy_l)) = self.green.assemble()?;
        let ((bx_h, bx_l), (by_h, by_l)) = self.blue.assemble()?;
        let ((wx_h, wx_l), (wy_h, wy_l)) = self.white.assemble()?;

        // TODO: Handle monochrome displays, store color values as 0x00

        // Encode bits
        #[rustfmt::skip]
        let bytes = [
            rx_l << 6 | ry_l << 4 | gx_l << 2 | gy_l,
            bx_l << 6 | by_l << 4 | wx_l << 2 | wy_l,
            rx_h,
            ry_h,
            gx_h,
            gy_h,
            bx_h,
            by_h,
            wx_h,
            wy_h,
        ];
        Ok(bytes)
    }
}

// SECTION 3.8 Established timings I and II

impl Assemble for EstablishedTiming {
    type Data = (usize, u8);

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use EstablishedTiming::*;
        let data = match self {
            H720V400F70 =>   (0, BIT7),
            H720V400F88 =>   (0, BIT6),
            H640V480F60 =>   (0, BIT5),
            H640V480F67 =>   (0, BIT4),
            H640V480F72 =>   (0, BIT3),
            H640V480F75 =>   (0, BIT2),
            H800V600F56 =>   (0, BIT1),
            H800V600F60 =>   (0, BIT0),
            H800V600F72 =>   (1, BIT7),
            H800V600F75 =>   (1, BIT6),
            H832V624F75 =>   (1, BIT5),
            H1024V768F87 =>  (1, BIT4),
            H1024V768F60 =>  (1, BIT3),
            H1024V768F70 =>  (1, BIT2),
            H1024V768F75 =>  (1, BIT1),
            H1280V1024F75 => (1, BIT0),
            H1152V870F75 =>  (2, BIT7),
        };
        Ok(data)
    }
}

impl Assemble for EstablishedTimings {
    type Data = [u8; 3];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // TODO: Improve reduction operation
        let reduction = |acc: [u8; 3], e: &(usize, u8)| {
            let mut res = acc;
            res[e.0] |= e.1;
            res
        };

        // TODO: Handle assembly errors in flat_map
        let data: [u8; 3] = self
            .established_timings
            .iter()
            .flat_map(Assemble::assemble)
            .collect::<Vec<(usize, u8)>>()
            .iter()
            .fold([0x00; 3], reduction);

        Ok(data)
    }
}

// SECTION 3.9 Standard timings

impl Assemble for AspectRatioSymbol {
    type Data = u8;

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use AspectRatioSymbol::*;
        let flags = match self {
            AR16TO10 => 0b00,
            AR4TO3 =>   0b01,
            AR5TO4 =>   0b10,
            AR16TO9 =>  0b11,
        };
        let data = flags << 6;
        Ok(data)
    }
}

impl Assemble for StandardTiming {
    type Data = [u8; 2];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let hres = ((self.horizontal_resolution / 8) - 31) as u8;

        let ar = self.aspect_ratio.assemble()?;
        let rr = (self.refresh_rate - 60) & 0b0011_1111;
        let byte2 = ar | rr;

        Ok([hres, byte2])
    }
}

impl Assemble for StandardTimings {
    type Data = [u8; 16];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Maximal number of blocks
        const MB: usize = 8;
        // Block size of a single block
        const BS: usize = 2;
        // Data filled with padding
        let mut data: [u8; MB * BS] = [0x01; MB * BS];

        for (i, st) in self.0.iter().take(MB).enumerate() {
            let st = st.assemble()?;
            for j in 0..BS {
                data[i * BS + j] = st[j];
            }
        }

        Ok(data)
    }
}

// SECTION 3.10 18 Byte descriptors

// SECTION 3.10.2 Detailed timing descriptor

impl Assemble for AddressableVideo {
    type Data = ((u8, u8), (u8, u8));

    #[allow(clippy::similar_names)]
    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let upper4 = |x: u16| -> u8 { ((x & 0b0000_1111_00000000) >> 8) as u8 };
        let lower8 = |x: u16| -> u8 { (x &  0b0000_0000_11111111) as u8 };

        let avh_h = upper4(self.horizontal);
        let avh_l = lower8(self.horizontal);
        let avv_h = upper4(self.vertical);
        let avv_l = lower8(self.vertical);
        Ok(((avh_h, avh_l), (avv_h, avv_l)))
    }
}

#[allow(clippy::similar_names)]
impl Assemble for Blanking {
    type Data = ((u8, u8), (u8, u8));

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let upper4 = |x: u16| -> u8 { ((x & 0b0000_1111_00000000) >> 8) as u8 };
        let lower8 = |x: u16| -> u8 { (x &  0b0000_0000_11111111) as u8 };

        let blh_h = upper4(self.horizontal);
        let blh_l = lower8(self.horizontal);
        let blv_h = upper4(self.vertical);
        let blv_l = lower8(self.vertical);
        Ok(((blh_h, blh_l), (blv_h, blv_l)))
    }
}

#[allow(clippy::similar_names)]
fn assemble_addressable_video_and_blanking(
    addressable_video: AddressableVideo,
    blanking: Blanking,
) -> Result<[u8; 6], AssemblyError> {
    let ((avh_h, avh_l), (avv_h, avv_l)) = addressable_video.assemble()?;
    let ((blh_h, blh_l), (blv_h, blv_l)) = blanking.assemble()?;

    #[rustfmt::skip]
    let bytes = [
        avh_l,
        blh_l,
        avh_h << 4 | blh_h,
        avv_l,
        blv_l,
        avv_h << 4 | blv_h
    ];
    Ok(bytes)
}

impl Assemble for FrontPorch {
    type Data = ((u8, u8), (u8, u8));

    #[allow(clippy::similar_names)]
    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let porchh_h = ((self.horizontal & 0b000000_11_00000000) >> 8) as u8;
        let porchh_l = ( self.horizontal & 0b000000_00_11111111) as u8;
        let porchv_h = ( self.vertical   & 0b0000000000_11_0000) >> 4;
        let porchv_l =   self.vertical   & 0b0000000000_00_1111;
        Ok(((porchh_h, porchh_l), (porchv_h, porchv_l)))
    }
}

impl Assemble for SyncPulseWidth {
    type Data = ((u8, u8), (u8, u8));

    #[allow(clippy::similar_names)]
    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let synch_h = ((self.horizontal & 0b000000_11_00000000) >> 8) as u8;
        let synch_l = ( self.horizontal & 0b000000_00_11111111) as u8;
        let syncv_h = ( self.vertical   & 0b0000000000_11_0000) >> 4;
        let syncv_l =   self.vertical   & 0b0000000000_00_1111;
        Ok(((synch_h, synch_l), (syncv_h, syncv_l)))
    }
}

#[allow(clippy::similar_names)]
fn assemble_front_porch_and_sync_pulse_width(
    front_porch: FrontPorch,
    sync_pulse_width: SyncPulseWidth,
) -> Result<[u8; 4], AssemblyError> {
    let ((porchh_h, porchh_l), (porchv_h, porchv_l)) = front_porch.assemble()?;
    let ((synch_h, synch_l), (syncv_h, syncv_l)) = sync_pulse_width.assemble()?;

    #[rustfmt::skip]
    let bytes = [
        porchh_l,
        synch_l,
        porchv_l << 4 | syncv_l,
        porchh_h << 6 | synch_h << 4 | porchv_h << 2 | syncv_h,
    ];
    Ok(bytes)
}

impl Assemble for VideoImageSize {
    type Data = [u8; 3];

    #[allow(clippy::similar_names)]
    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let upper4 = |x: u16| -> u8 { ((x & 0b0000_1111_00000000) >> 8) as u8 };
        let lower8 = |x: u16| -> u8 { ( x & 0b0000_0000_11111111) as u8 };

        let sizeh_h = upper4(self.width);
        let sizeh_l = lower8(self.width);
        let sizev_h = upper4(self.height);
        let sizev_l = lower8(self.height);

        #[rustfmt::skip]
        let bytes = [
            sizeh_l,
            sizev_l,
            sizeh_h << 4 | sizev_h
        ];
        Ok(bytes)
    }
}

impl Assemble for BorderSize {
    type Data = [u8; 2];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        Ok([self.left, self.top])
    }
}

impl Assemble for SignalInterface {
    type Data = u8;

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use SignalInterface::*;
        let flags = match self {
            NonInterlaced => 0b0,
            Interlaced =>    0b1,
        };
        let data = (flags << 7) & BIT7;
        Ok(data)
    }
}

impl Assemble for StereoViewingType {
    type Data = u8;

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use StereoViewingType::*;
        let flags = match self {
            NoStereo =>                  0b0_00_0000_0,
            DoNotCare =>                 0b0_00_0000_1,
            SequentialRightSync =>       0b0_01_0000_0,
            SequentialLeftSync =>        0b0_10_0000_0,
            InterleavedLinesRightEven => 0b0_01_0000_1,
            InterleavedLinesLeftEven =>  0b0_10_0000_1,
            Interleaved4Way =>           0b0_11_0000_0,
            SideBySide =>                0b0_11_0000_1,
        };
        let data = flags & (BIT6 | BIT5 | BIT0);
        Ok(data)
    }
}

impl Assemble for SyncSignal {
    type Data = u8;

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use SyncSignal::*;
        let flags = match self {
            AnalogComposite =>        0b0,
            BipolarAnalogComposite => 0b1,
        };
        Ok(flags)
    }
}

impl Assemble for SyncLine {
    type Data = u8;

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use SyncLine::*;
        let flags = match self {
            RGB =>   0b1,
            Green => 0b0,
        };
        Ok(flags)
    }
}

impl Assemble for SyncPolarity {
    type Data = u8;

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use SyncPolarity::*;
        let flags = match self {
            Positive => 0b1,
            Negative => 0b0,
        };
        Ok(flags)
    }
}

impl Assemble for SyncSignalDefinition {
    type Data = u8;

    #[allow(clippy::identity_op)]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use SyncSignalDefinition::*;
        let flags = match self {
            AnalogComposite {
                sync,
                serrations,
                line,
            } => {
                let sy = sync.assemble()?;
                let se = u8::from(*serrations);
                let li = line.assemble()?;
                0b0 << 4 | sy << 3 | se << 2 | li << 1
            }
            DigitalComposite {
                serrations,
                horizontal,
            } => {
                let se = u8::from(*serrations);
                let ho = horizontal.assemble()?;
                0b1 << 4 | 0b0 << 3 | se << 2 | ho << 1
            }
            DigitalSeparate { horizontal, vertical } => {
                let ho = horizontal.assemble()?;
                let ve = vertical.assemble()?;
                0b1 << 4 | 0b1 << 3 | ve << 2 | ho << 1
            }
        };
        let data = flags & (BIT4 | BIT3 | BIT2 | BIT1);
        Ok(data)
    }
}

fn assemble_detailed_timing_flags(
    signal_interface: SignalInterface,
    stereo_viewing: StereoViewingType,
    sync_signal_type: SyncSignalDefinition,
) -> Result<[u8; 1], AssemblyError> {
    let flags1 = signal_interface.assemble()?;
    let flags2 = stereo_viewing.assemble()?;
    let flags3 = sync_signal_type.assemble()?;
    let flags = [flags1 | flags2 | flags3];
    Ok(flags)
}

impl Assemble for DetailedTiming {
    type Data = [u8; 18];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Pixel Clock
        let pxcl = (self.pixel_clock / 10 / kHz) as u16;
        let pxclb: [u8; 2] = pxcl.to_le_bytes();
        assert_ne!(pxclb, [0x00, 0x00]);

        // Addressable video in pixels and lines.
        // Blanking in pixels and lines.
        let av_b = assemble_addressable_video_and_blanking(self.addressable_video, self.blanking)?;

        // Front porch in pixels and lines.
        // Sync pulse width in pixels and lines.
        let fp_pw = assemble_front_porch_and_sync_pulse_width(self.front_porch, self.sync_pulse_width)?;

        // Video image size in milimeters.
        let vis = self.video_image_size.assemble()?;

        // Border size in pixels and lines.
        let bs = self.border_size.assemble()?;

        // DetailedTiming flags.
        let flags = assemble_detailed_timing_flags(
            self.signal_interface,
            self.stereo_viewing,
            self.sync_signal_type,
        )?;

        let result = concat_arrays!(pxclb, av_b, fp_pw, vis, bs, flags);
        Ok(result)
    }
}

impl Assemble for DetailedTimings {
    type Data = Vec<[u8; 18]>;

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // TODO: Handle assembly errors in flat_map
        let data = self.0.iter().flat_map(Assemble::assemble).collect();
        Ok(data)
    }
}

// SECTION 3.10.3 Display descriptor definitions

// Display product serial number.
impl Assemble for ProductSerialNumber {
    type Data = [u8; 18];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Bytes 0, ..., 4
        let tag = 0xff;
        let header = [0x00, 0x00, 0x00, tag, 0x00];

        // Bytes 5, ..., 17
        // Restrict to ASCII
        // TODO: Error for non-ASCII characters
        let mut string = self.serial_number.clone();
        string.retain(|c| c.is_ascii());
        let mut data = string.as_bytes().to_vec();

        // Truncate to max length or add padding if necessary
        let maxlen = 13;
        data.push(0x0a);
        data.resize(maxlen, 0x20);

        let data: [u8; 13] = data.try_into().unwrap();
        let result = concat_arrays!(header, data);
        Ok(result)
    }
}

// Alphanumeric data string.
impl Assemble for AlphanumericDataString {
    type Data = [u8; 18];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Bytes 0, ..., 4
        let tag = 0xfe;
        let header = [0x00, 0x00, 0x00, tag, 0x00];

        // Bytes 5, ..., 17
        // Restrict to ASCII
        // TODO: Error for non-ASCII characters
        let mut string = self.string.clone();
        string.retain(|c| c.is_ascii());
        let mut data = string.as_bytes().to_vec();

        // Truncate to max length or add padding if necessary
        let maxlen = 13;
        data.push(0x0a);
        data.resize(maxlen, 0x20);

        let data: [u8; 13] = data.try_into().unwrap();
        let result = concat_arrays!(header, data);
        Ok(result)
    }
}

// Display range limits.
impl Assemble for RangeLimitOffset {
    type Data = u8;

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use RangeLimitOffset::*;
        let data = match self {
            None =>   0b00,
            Max =>    0b10,
            MinMax => 0b11,
        };
        Ok(data)
    }
}

impl Assemble for RangeLimitOffsets {
    type Data = [u8; 1];

    #[allow(clippy::identity_op, clippy::similar_names)]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let ofh = self.horizontal.assemble()?;
        let ofv = self.vertical.assemble()?;
        let data = 0b0000_0000 | ofh << 2 | ofv;
        Ok([data])
    }
}

fn assemble_rates_offset_helper(range: MinMaxRange<u32>, offset: RangeLimitOffset, divider: u32) -> [u8; 2] {
    use RangeLimitOffset::*;
    let (min, max) = match offset {
        None => (range.min / divider, range.max / divider),
        Max => (range.min / divider, range.max / divider - 255),
        MinMax => (range.min / divider - 255, range.max / divider - 255),
    };
    [min as u8, max as u8]
}

impl Assemble for TimingSupportFlags {
    type Data = [u8; 1];

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use TimingSupportFlags::*;
        let data = match self {
            DefaultGTF =>      0x00,
            RangeLimitsOnly => 0x01,
            SecondaryGTF =>    0x02,
            CVTSupport =>      0x04,
        };
        Ok([data])
    }
}

impl Assemble for GTFCurveDefinition {
    type Data = [u8; 7];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Byte 11
        let reserved = 0x00;
        // Byte 12
        let bf = (self.break_frequency / 2 / kHz) as u8;
        // Bytes 13, ..., 17
        let c = self.c * 2;
        let m = self.m.to_le_bytes();
        let k = self.k;
        let j = self.j * 2;

        Ok([reserved, bf, c, m[0], m[1], k, j])
    }
}

impl Assemble for ActivePixels {
    type Data = (u8, u8);

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use ActivePixels::*;
        let ap = match self {
            Unlimited => 0x00_00,
            Maximum(p) => p / 8,
        };
        let ap_h = ((ap & 0b000000_11_00000000) >> 8) as u8;
        let ap_l = ( ap & 0b000000_00_11111111) as u8;
        Ok((ap_h, ap_l))
    }
}

impl Assemble for CVTAspectRatio {
    type Data = u8;

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use CVTAspectRatio::*;
        let data = match self {
            AR4TO3 =>   0b000,
            AR16TO9 =>  0b001,
            AR16TO10 => 0b010,
            AR5TO4 =>   0b011,
            AR15TO9 =>  0b100,
        };
        Ok(data)
    }
}

impl Assemble for Vec<CVTAspectRatio> {
    type Data = u8;

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use CVTAspectRatio::*;
        let to_pattern = |ar: &CVTAspectRatio| match ar {
            AR4TO3 =>   0b10000,
            AR16TO9 =>  0b01000,
            AR16TO10 => 0b00100,
            AR5TO4 =>   0b00010,
            AR15TO9 =>  0b00001,
        };

        let data = self
            .iter()
            .map(to_pattern)
            .collect::<Vec<u8>>()
            .iter()
            .fold(0x00, |acc: u8, e: &u8| acc | e);

        Ok(data)
    }
}

impl Assemble for CVTBlankingSupport {
    type Data = u8;

    #[allow(clippy::similar_names)]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let sbs = u8::from(self.standard);
        let rbs = u8::from(self.reduced);
        let data = rbs << 1 | sbs;
        Ok(data)
    }
}

impl Assemble for CVTDisplayScaling {
    type Data = u8;

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use CVTDisplayScaling::*;
        let data = match self {
            HorizontalShrink =>  0b1000,
            HorizontalStretch => 0b0100,
            VerticalShrink =>    0b0010,
            VerticalStretch =>   0b0001,
        };
        Ok(data)
    }
}

impl Assemble for Vec<CVTDisplayScaling> {
    type Data = u8;

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let data = self
            .iter()
            .flat_map(Assemble::assemble)
            .collect::<Vec<u8>>()
            .iter()
            .fold(0x00, |acc: u8, e: &u8| acc | e);
        Ok(data)
    }
}

impl Assemble for CVTDefinition {
    type Data = [u8; 7];

    #[allow(clippy::similar_names)]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Byte 11
        let (ver, rev) = self.standard_version;
        // Bytes 12, 13
        let precision = (self.pixel_clock_precision * 4 / MHz) as u8;
        let pclk = precision & 0b00_111111;
        let (ap_h, ap_l) = self.active_pixels_per_line.assemble()?;
        // Byte 14
        let sar = self.supported_aspect_ratios.assemble()?;
        // Byte 15
        let par = self.preferred_aspect_ratio.assemble()?;
        let bs = self.blanking_support.assemble()?;
        // Byte 16
        let ds = self.display_scaling_type.assemble()?;
        // Byte 17
        let pvrr = self.preferred_vertical_refresh_rate;
        assert_ne!(pvrr, 0x00);

        #[rustfmt::skip]
        let data = [
            ver << 4 | rev,
            pclk << 2 | ap_h,
            ap_l,
            sar << 3,
            par << 5 | bs << 3,
            ds << 4,
            pvrr,
        ];
        Ok(data)
    }
}

impl Assemble for VideoTimingData {
    type Data = [u8; 7];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use VideoTimingData::*;
        let data = match self {
            None => [0x0a, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20],
            GTF(gtf) => gtf.assemble()?,
            CVT(cvt) => cvt.assemble()?,
        };
        Ok(data)
    }
}

impl Assemble for RangeLimits {
    type Data = [u8; 18];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Bytes 0, ..., 3
        let tag = 0xfd;
        let header = [0x00, 0x00, 0x00, tag];
        // Byte 4
        let range_limits_offsets: [u8; 1] = self.range_limits_offsets.assemble()?;
        // Bytes 5, ..., 8
        let vertical_rate: [u8; 2] =
            assemble_rates_offset_helper(self.vertical_rate, self.range_limits_offsets.vertical, Hz);
        let horizontal_rate: [u8; 2] =
            assemble_rates_offset_helper(self.horizontal_rate, self.range_limits_offsets.horizontal, kHz);
        // Byte 9
        let max_pixel_clock = [(self.max_pixel_clock / 10 / MHz) as u8];
        // Byte 10
        let timing_support_flags: [u8; 1] = self.timing_support_flags.assemble()?;
        // Bytes 11, ..., 17
        let video_timing_data: [u8; 7] = self.video_timing_data.assemble()?;

        let result = concat_arrays!(
            header,
            range_limits_offsets,
            vertical_rate,
            horizontal_rate,
            max_pixel_clock,
            timing_support_flags,
            video_timing_data
        );
        Ok(result)
    }
}

// Display product name.
impl Assemble for ProductName {
    type Data = [u8; 18];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Bytes 0, ..., 4
        let tag = 0xfc;
        let header = [0x00, 0x00, 0x00, tag, 0x00];

        // Bytes 5, ..., 17
        // Restrict to ASCII
        // TODO: Error for non-ASCII characters
        let mut string = self.name.clone();
        string.retain(|c| c.is_ascii());
        let mut data = string.as_bytes().to_vec();

        // Truncate to max length or add padding if necessary
        let maxlen = 13;
        data.push(0x0a);
        data.resize(maxlen, 0x20);

        let data: [u8; 13] = data.try_into().unwrap();
        let result = concat_arrays!(header, data);
        Ok(result)
    }
}

// Color point data.
impl Assemble for WhitePointDatum {
    type Data = [u8; 5];

    #[allow(clippy::identity_op, clippy::similar_names)]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        let index = self.index;
        let ((wx_h, wx_l), (wy_h, wy_l)) = self.white_point.assemble()?;
        let gamma = self.gamma.assemble()?;

        assert_ne!(index, 0x00);

        #[rustfmt::skip]
        let result = concat_arrays!(
            [
                index,
                0b0000 | wx_l << 2 | wy_l,
                wx_h, wy_h
            ],
            gamma);
        Ok(result)
    }
}

impl Assemble for ColorPointData {
    type Data = [u8; 18];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Maximal number of blocks
        const MB: usize = 2;
        // Block size of a single block
        const BS: usize = 5;

        // Bytes 0, ..., 4
        let tag = 0xfb;
        let header = [0x00, 0x00, 0x00, tag, 0x00];

        // Bytes 5, ..., 14
        // Data filled with padding
        let mut data: [u8; MB * BS] = [0x00; MB * BS];

        for (i, st) in self.white_points.iter().take(MB).enumerate() {
            let st = st.assemble()?;
            for j in 0..BS {
                data[i * BS + j] = st[j];
            }
        }

        // Bytes 15, 16, 17
        let padding = [0x0a, 0x20, 0x20];

        let result = concat_arrays!(header, data, padding);
        Ok(result)
    }
}

// Standard timing identifiers.
impl Assemble for StandardTimingIdentifiers {
    type Data = [u8; 18];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Maximal number of blocks
        const MB: usize = 6;
        // Block size of a single block
        const BS: usize = 2;

        // Bytes 0, ..., 4
        let tag = 0xfa;
        let header = [0x00, 0x00, 0x00, tag, 0x00];

        // Bytes 5, ..., 16
        // Data filled with padding
        let mut data: [u8; MB * BS] = [0x01; MB * BS];

        for (i, st) in self.timings.iter().take(MB).enumerate() {
            let st = st.assemble()?;
            for j in 0..BS {
                data[i * BS + j] = st[j];
            }
        }

        // Byte 17
        let padding = [0x0a];

        let result = concat_arrays!(header, data, padding);
        Ok(result)
    }
}

// Display color management data.
impl Assemble for ColorManagementData {
    type Data = [u8; 18];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Bytes 0, ..., 4
        let tag = 0xf9;
        let header = [0x00, 0x00, 0x00, tag, 0x00];
        // Byte 5
        // assert_eq!(self.version, 0x03);
        // let version = [self.version];
        let version = [0x03];
        // Bytes 6, ..., 17
        let r3 = self.red.0.to_le_bytes();
        let r2 = self.red.1.to_le_bytes();
        let g3 = self.green.0.to_le_bytes();
        let g2 = self.green.1.to_le_bytes();
        let b3 = self.blue.0.to_le_bytes();
        let b2 = self.blue.1.to_le_bytes();

        let data: [u8; 12] = concat_arrays!(r3, r2, g3, g2, b3, b2);
        let result = concat_arrays!(header, version, data);
        Ok(result)
    }
}

// CVT 3-byte timing codes.
impl Assemble for VerticalRate {
    type Data = u8;

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use VerticalRate::*;
        let flags = match self {
            RR50 => 0b00,
            RR60 => 0b01,
            RR75 => 0b10,
            RR85 => 0b11,
        };
        Ok(flags)
    }
}

impl Assemble for Vec<(VerticalRate, BlankingType)> {
    type Data = u8;

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use BlankingType::*;
        use VerticalRate::*;
        let to_pattern = |item: &(VerticalRate, BlankingType)| {
            match item {
                (RR50, Standard) => 0b10000,
                (RR60, Standard) => 0b01000,
                (RR75, Standard) => 0b00100,
                (RR85, Standard) => 0b00010,
                (RR60, Reduced) =>  0b00001,
                _ => unreachable!(), // Not defined in standard
            }
        };

        let flags = self
            .iter()
            .map(to_pattern)
            .collect::<Vec<u8>>()
            .iter()
            .fold(0x00, |acc: u8, e: &u8| acc | e);

        let data = flags & (BIT4 | BIT3 | BIT2 | BIT1 | BIT1);
        Ok(data)
    }
}

impl Assemble for CVTTimingCode {
    type Data = [u8; 3];

    #[allow(clippy::match_wildcard_for_single_variants)]
    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use CVTAspectRatio::*;
        // Raw value is encoded as lines_per_field / 2 - 1.
        let encode = |value: u16| (value / 2) - 1;

        let lpf = encode(self.lines_per_field);
        let lpf_h = ((lpf & 0b0000_1111_00000000) >> 8) as u8;
        let lpf_l = (lpf  & 0b0000_0000_11111111) as u8;

        // Note: Yet another aspect ratio encoding!
        let ar = match self.aspect_ratio {
            AR4TO3 =>   0b00,
            AR16TO9 =>  0b01,
            AR16TO10 => 0b10,
            AR15TO9 =>  0b11,
            _ => unreachable!(), // Not all values are represented.
        };

        let reserved = 0b00;

        let pvr = self.preferred_vertical_rate.assemble()?;
        let svrbs = self.supported_vertical_rates.assemble()?;

        let bytes = [
            lpf_l,
            lpf_h << 4 | ar << 2 | reserved,
            pvr << 5 | svrbs];
        Ok(bytes)
    }
}

impl Assemble for CVTTimingCodes {
    type Data = [u8; 18];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Maximal number of blocks
        const MB: usize = 4;
        // Block size of a single block
        const BS: usize = 3;

        // Bytes 0, ..., 4
        let tag = 0xf8;
        let header = [0x00, 0x00, 0x00, tag, 0x00];

        // Byte 5
        // assert_eq!(self.version, 0x01);
        // let version = [self.version];
        let version = [0x01];

        // Bytes 6, ..., 17
        // Data filled with padding
        let mut data: [u8; MB * BS] = [0x00; MB * BS];

        for (i, st) in self.cvt_codes.iter().take(MB).enumerate() {
            let st = st.assemble()?;
            for j in 0..BS {
                data[i * BS + j] = st[j];
            }
        }

        let result = concat_arrays!(header, version, data);
        Ok(result)
    }
}

// Established timings III.
impl Assemble for EstablishedTimingIII {
    type Data = (usize, u8);

    #[rustfmt::skip]
    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use EstablishedTimingIII::*;
        let data = match self {
            H640V350F85 =>     (0, BIT7),
            H640V400F85 =>     (0, BIT6),
            H720V400F85 =>     (0, BIT5),
            H640V480F85 =>     (0, BIT4),
            H848V480F60 =>     (0, BIT3),
            H800V600F85 =>     (0, BIT2),
            H1024V768F85 =>    (0, BIT1),
            H1152V864F75 =>    (0, BIT0),
            H1280V768F60RB =>  (1, BIT7),
            H1280V768F60 =>    (1, BIT6),
            H1280V768F75 =>    (1, BIT5),
            H1280V768F85 =>    (1, BIT4),
            H1280V960F60 =>    (1, BIT3),
            H1280V960F85 =>    (1, BIT2),
            H1280V1024F60 =>   (1, BIT1),
            H1280V1024F85 =>   (1, BIT0),
            H1360V768F60 =>    (2, BIT7),
            H1440V900F60RB =>  (2, BIT6),
            H1440V900F60 =>    (2, BIT5),
            H1440V900F75 =>    (2, BIT4),
            H1440V900F85 =>    (2, BIT3),
            H1400V1050F60RB => (2, BIT2),
            H1400V1050F60 =>   (2, BIT1),
            H1400V1050F75 =>   (2, BIT0),
            H1400V1050F85 =>   (3, BIT7),
            H1680V1050F60RB => (3, BIT6),
            H1680V1050F60 =>   (3, BIT5),
            H1680V1050F75 =>   (3, BIT4),
            H1680V1050F85 =>   (3, BIT3),
            H1600V1200F60 =>   (3, BIT2),
            H1600V1200F65 =>   (3, BIT1),
            H1600V1200F70 =>   (3, BIT0),
            H1600V1200F75 =>   (4, BIT7),
            H1600V1200F85 =>   (4, BIT6),
            H1792V1344F60 =>   (4, BIT5),
            H1792V1344F75 =>   (4, BIT4),
            H1856V1392F60 =>   (4, BIT3),
            H1856V1392F75 =>   (4, BIT2),
            H1920V1200F60RB => (4, BIT1),
            H1920V1200F60 =>   (4, BIT0),
            H1920V1200F75 =>   (5, BIT7),
            H1920V1200F85 =>   (5, BIT6),
            H1920V1440F60 =>   (5, BIT5),
            H1920V1440F75 =>   (5, BIT4),
        };
        Ok(data)
    }
}

impl Assemble for EstablishedTimingsIII {
    type Data = [u8; 18];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Bytes 0, ..., 4
        let tag = 0xf7;
        let header = [0x00, 0x00, 0x00, tag, 0x00];

        // Byte 5
        // assert_eq!(self.version, 0x0a);
        // let version = [self.version];
        let version = [0x0a];

        // Bytes 6, ..., 11
        // TODO: Improve reduction operation
        let reduction = |acc: [u8; 6], e: &(usize, u8)| {
            let mut res = acc;
            res[e.0] |= e.1;
            res
        };

        // TODO: Handle assembly errors in flat_map
        let data: [u8; 6] = self
            .timings
            .iter()
            .flat_map(Assemble::assemble)
            .collect::<Vec<(usize, u8)>>()
            .iter()
            .fold([0x00; 6], reduction);

        // Bytes 12, ..., 17
        let padding = [0x00; 6];

        let result = concat_arrays!(header, version, data, padding);
        Ok(result)
    }
}

// Reserved: currently undefined.
impl Assemble for ReservedDescriptor {
    type Data = [u8; 18];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Bytes 0, ..., 4
        let tag = self.tag;
        let header = [0x00, 0x00, 0x00, tag, 0x00];
        assert!((0x11..=0xf6).contains(&tag));

        // Bytes 5, ..., 17
        // TODO: Take or drop the reserved bytes?
        // let data: [u8; 13] = self.bytes;
        let data: [u8; 13] = [0x00; 13];

        let result = concat_arrays!(header, data);
        Ok(result)
    }
}

// Dummy descriptor.
impl Assemble for DummyDescriptor {
    type Data = [u8; 18];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Bytes 0, ..., 4
        let tag = 0x10;
        let header = [0x00, 0x00, 0x00, tag, 0x00];
        // Bytes 5, ..., 17
        let data = [0x00; 13];

        let result = concat_arrays!(header, data);
        Ok(result)
    }
}

// Manufacturer specified data.
impl Assemble for ManufacturerSpecified {
    type Data = [u8; 18];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Bytes 0, ..., 4
        let tag = self.tag;
        let header = [0x00, 0x00, 0x00, tag, 0x00];
        assert!((0x00..=0x0f).contains(&tag));

        // Bytes 5, ..., 17
        // let data = self.bytes;
        // TODO: Proper error handling
        let Ok(bytes) = decode(self.bytes.clone()) else { unreachable!() };
        let data: [u8; 13] = bytes.try_into().unwrap();

        let result = concat_arrays!(header, data);
        Ok(result)
    }
}

impl Assemble for DisplayDescriptor {
    type Data = [u8; 18];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        use DisplayDescriptor::*;
        let data = match self {
            // Descriptor tag: `0xff`.
            ProductSerialNumber(d) => d.assemble()?,
            // Descriptor tag: `0xfe`.
            AlphanumericDataString(d) => d.assemble()?,
            // Descriptor tag: `0xfe`.
            RangeLimits(d) => d.assemble()?,
            // Descriptor tag: `0xfc`.
            ProductName(d) => d.assemble()?,
            // Descriptor tag: `0xfb`.
            ColorPointData(d) => d.assemble()?,
            // Descriptor tag: `0xfa`.
            StandardTimingIdentifiers(d) => d.assemble()?,
            // Descriptor tag: `0xf9`.
            ColorManagementData(d) => d.assemble()?,
            // Descriptor tag: `0xf8`.
            CVTTimingCodes(d) => d.assemble()?,
            // Descriptor tag: `0xf7`.
            EstablishedTimingsIII(d) => d.assemble()?,
            // Descriptor tags: `0xf6 .. 0x11`
            ReservedDescriptor(d) => d.assemble()?,
            // Descriptor tag: `0x10`.
            DummyDescriptor(d) => d.assemble()?,
            // Descriptor tags: `0x0f .. 0x00`
            ManufacturerSpecified(d) => d.assemble()?,
        };
        Ok(data)
    }
}

impl Assemble for DisplayDescriptors {
    type Data = Vec<[u8; 18]>;

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // TODO: Handle assembly errors in flat_map
        let data = self.0.iter().flat_map(Assemble::assemble).collect();
        Ok(data)
    }
}

// SECTION 3.11 Extension Flag

#[allow(clippy::similar_names)]
fn assemble_18byte_blocks_helper(
    detailed_timings: &DetailedTimings,
    display_descriptors: &DisplayDescriptors,
) -> Result<[u8; 4 * 18], AssemblyError> {
    // Maximal number of blocks
    const MB: usize = 4;
    // Block size of a single block
    const BS: usize = 18;

    let dtd = detailed_timings.assemble()?;
    let did = display_descriptors.assemble()?;

    // Append padding if necessary
    let n = MB - (dtd.len() + did.len());
    let dd = DummyDescriptor {};
    let ddd: Vec<[u8; BS]> = (0..n).flat_map(|_| dd.assemble()).collect();

    // Combine all descriptors
    let ds = dtd.iter().chain(did.iter()).chain(ddd.iter());

    // Fill in data
    let mut data: [u8; MB * BS] = [0x00; MB * BS];
    for (i, st) in ds.take(MB).enumerate() {
        for j in 0..BS {
            data[i * BS + j] = st[j];
        }
    }

    Ok(data)
}

impl Assemble for EDIDBaseBlock {
    // type Data = Vec<u8>;
    type Data = [u8; BLOCKSIZE];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // The EDID magic bytes header.
        let header = self.header.assemble()?;

        // Vendor and product identification.
        let product = self.product.assemble()?;

        // The EDID structure version and revision.
        let version = self.version.assemble()?;

        // Basic display parameters and features.
        let display_parameters = self.display_parameters.assemble()?;

        // Display x,y chromaticity coordinates.
        let chromaticity = self.chromaticity.assemble()?;

        // Established timings
        let established_timings = self.established_timings.assemble()?;

        // Standard timings
        let standard_timings = self.standard_timings.assemble()?;

        // Detailed timing descriptors.
        // and
        // Display descriptors.
        let descriptors = assemble_18byte_blocks_helper(&self.detailed_timings, &self.display_descriptors)?;

        // Extension block count
        let extension_count: [u8; 1] = [self.extension_count];

        // Concat all the pieces
        let data: [u8; BLOCKSIZE - 1] = concat_arrays!(
            header,
            product,
            version,
            display_parameters,
            chromaticity,
            established_timings,
            standard_timings,
            descriptors,
            extension_count
        );

        // Checksum
        let checksum: [u8; 1] = [construct_checksum(&data)];

        // Append checksum
        Ok(concat_arrays!(data, checksum))
    }
}
