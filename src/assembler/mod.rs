/// Type definitions for assembling.
pub mod types;

/// The assembler for complete EDID binary blobs.
pub mod edid;

/// The assembler for the EDID base block.
pub mod base_block;
/// The assembler for the CEA extension block.
pub mod cea_block;
