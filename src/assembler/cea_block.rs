#![allow(clippy::drop_non_drop)]
#![allow(clippy::wildcard_imports)]

use concat_arrays::concat_arrays;

use crate::assembler::types::{Assemble, AssemblyError};
use crate::checksum::construct_checksum;
use crate::datamodel::cea_block::{EDIDExtensionBlockCEA, BLOCK_TAG};
use crate::definitions::*;

impl Assemble for EDIDExtensionBlockCEA {
    type Data = [u8; BLOCKSIZE];

    fn assemble(&self) -> Result<Self::Data, AssemblyError> {
        // Byte 0
        let tag = [BLOCK_TAG];

        // Byte 1
        let version = [self.version];

        // TODO: Assemble CEA internals
        let padding = [0x00; BLOCKSIZE - 3];

        // Concat all the pieces
        let data: [u8; BLOCKSIZE - 1] = concat_arrays!(tag, version, padding);

        // Checksum
        let checksum: [u8; 1] = [construct_checksum(&data)];

        // Append checksum
        Ok(concat_arrays!(data, checksum))
    }
}
