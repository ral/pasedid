use crate::assembler::types::{Assemble, AssemblyError};
use crate::config::Config;
use crate::datamodel::edid::{EDIDExtensionBlock, EDID};
use crate::definitions::BLOCKSIZE;

fn assemble_edid_extension_block(block: &EDIDExtensionBlock) -> Result<[u8; BLOCKSIZE], AssemblyError> {
    use EDIDExtensionBlock::*;

    // TODO: Handle more block types
    let bytes = match block {
        CEA(b) => b.assemble()?,
        _ => todo!(),
    };

    Ok(bytes)
}

/// Assembler for a complete EDID binary blob.
pub fn assemble_edid(blocks: &EDID, cfg: Config) -> Result<Vec<u8>, AssemblyError> {
    // Assemble base block
    let base_block = blocks.base_block.assemble()?.to_vec();

    // Assemble extension blocks
    // TODO: Handle assembly errors in flat_map
    let extension_blocks = match cfg.handle_extensions {
        true => blocks
            .extension_blocks
            .iter()
            .flat_map(assemble_edid_extension_block)
            .collect::<Vec<[u8; BLOCKSIZE]>>()
            .concat(),
        false => vec![],
    };

    // Return simple concat of all blocks
    Ok([base_block, extension_blocks].concat())
}
