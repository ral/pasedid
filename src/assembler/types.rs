use std::error::Error;
use std::fmt;

/// Assemble trait.
pub trait Assemble {
    /// Type of data returned by the `assemble` function.
    type Data;

    /// Assemble a data model structure into its binary blob form according to the EDID specification.
    fn assemble(&self) -> Result<Self::Data, AssemblyError>
    where
        Self: Sized;
}

/// Assembly error type.
#[derive(Debug)]
pub struct AssemblyError {
    /// Error message.
    pub message: String,
}

impl Error for AssemblyError {}

impl fmt::Display for AssemblyError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl From<std::io::Error> for AssemblyError {
    fn from(e: std::io::Error) -> Self {
        AssemblyError {
            message: e.to_string(),
        }
    }
}
