use crate::checksum::print_checksum;
use crate::definitions::BLOCKSIZE;

/// Read an EDID binary blob block by block. Check the first block for the
/// number of extension blocks that should follow and try to read them all.
pub fn block_by_block_read(
    blocks: Option<usize>,
    read: impl Fn(u8, usize) -> Result<Vec<u8>, std::io::Error>,
) -> Result<Vec<u8>, std::io::Error> {
    match blocks {
        // Default: autodetect number of extension blocks
        None => {
            // Read first block (128 bytes)
            let data_base = read(0x00, BLOCKSIZE)?;
            print_checksum(data_base.as_slice());

            // Try to read remaining bytes
            let data_extensions = match data_base.len() {
                // Proper first block (128 bytes)
                BLOCKSIZE => {
                    // Number of extension blocks that should follow
                    let num_extensions: usize = data_base[BLOCKSIZE - 2].into();
                    // Read remaining blocks
                    match num_extensions {
                        0 => vec![],
                        #[allow(clippy::cast_possible_truncation)]
                        _ => read(BLOCKSIZE as u8, num_extensions * BLOCKSIZE)?,
                    }
                }
                // Something else, probably broken
                _ => vec![],
            };
            data_extensions.chunks_exact(BLOCKSIZE).for_each(print_checksum);

            // Return complete byte stream
            Ok([data_base, data_extensions].concat())
        }

        // Explicitly read as many blocks as specified by user
        Some(blocks) => {
            // Read blocks
            let data = read(0x00, blocks * BLOCKSIZE)?;
            data.chunks_exact(BLOCKSIZE).for_each(print_checksum);

            // Return complete byte stream
            Ok(data)
        }
    }
}
