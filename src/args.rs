use clap::Parser;

/// Command line argument definitions.
#[derive(Parser, Debug)]
#[command(version, about, long_about = None, next_line_help = true)]
pub struct Args {
    /// Number of 128-byte blocks to read, defaults to autodetect from data.
    #[arg(short, long, default_value = None)]
    pub blocks: Option<usize>,

    /// Enable handling of extension blocks.
    /// Support for extension blocks is highly incomplete!
    #[arg(short, long)]
    pub extensions: bool,
}
