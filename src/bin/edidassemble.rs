use std::io::{self, Write};

use clap::Parser;

use pasedid::args::Args;
use pasedid::assembler::edid::assemble_edid;
use pasedid::checksum::print_checksum;
use pasedid::config::Config;

fn main() -> Result<(), std::io::Error> {
    // Parse command line arguments
    let args = Args::parse();

    // Assembler configuration
    let cfg = Config {
        handle_extensions: args.extensions,
    };

    // Read yaml from stdin
    let yaml = io::read_to_string(io::stdin())?;

    // Deserialize yaml to data model
    let data = serde_yaml::from_str(&yaml);
    let Ok(model) = data else { panic!("Yaml file does not match data model") };

    // Serialize data model to binary
    let assembled = assemble_edid(&model, cfg);
    let Ok(bytes) = assembled else { panic!("Could not assemble binary. Invalid input?") };

    // Do not clobber stdout!
    eprintln!("Checksum of output data:");
    print_checksum(&bytes);

    // Write binary to stdout
    let mut stdout = io::stdout().lock();
    stdout.write_all(&bytes)
}
