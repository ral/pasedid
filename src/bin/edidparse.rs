use std::io::{self, Write};

use clap::Parser;

use pasedid::args::Args;
use pasedid::config::Config;
use pasedid::io::block_by_block_read;
use pasedid::parser::edid::parse_edid;
use pasedid::std::read_from_stdin;

fn main() -> Result<(), std::io::Error> {
    // Parse command line arguments
    let args = Args::parse();

    // Parser configuration
    let cfg = Config {
        handle_extensions: args.extensions,
    };

    // Read binary blob from stdin
    let bytes = block_by_block_read(args.blocks, |_, l| read_from_stdin(l))?;

    // Parse bytes into data model
    let parsed = parse_edid(bytes.as_slice(), cfg);
    let Ok((_, model)) = parsed else { panic!("Could not parse binary. Maybe invalid?") };

    // Serialize data model to yaml
    let yaml: std::string::String = serde_yaml::to_string(&model).unwrap();

    // Write yaml to stdout
    let mut stdout = io::stdout().lock();
    stdout.write_all(&yaml.into_bytes())
}
