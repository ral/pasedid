//! Parse and assemble EDID binary data blobs.

#![allow(clippy::match_bool)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::module_name_repetitions)]

/// The EDID data model.
pub mod datamodel;

/// The parser for EDID binary blobs.
pub mod parser;

/// The assembler for EDID binary blobs.
pub mod assembler;

/// Misc constant definitions.
pub mod definitions;

/// Command line argument definitions.
pub mod args;

/// Configuration for the parser and assembler.
pub mod config;

/// Checksum functions for EDID data.
pub mod checksum;

/// Abstract io for EDID blocks.
pub mod io;

/// Read from stdin and write to stdout.
pub mod std;
