#![allow(non_upper_case_globals)]
#![allow(non_snake_case)]
#![allow(dead_code)]
#![allow(clippy::doc_markdown)]

/// Size of an EDID block in bytes.
pub const BLOCKSIZE: usize = 128;

/// Bit pattern with only bit 0 set.
pub const BIT0: u8 = 0b0000_0001;
/// Bit pattern with only bit 1 set.
pub const BIT1: u8 = 0b0000_0010;
/// Bit pattern with only bit 2 set.
pub const BIT2: u8 = 0b0000_0100;
/// Bit pattern with only bit 3 set.
pub const BIT3: u8 = 0b0000_1000;
/// Bit pattern with only bit 4 set.
pub const BIT4: u8 = 0b0001_0000;
/// Bit pattern with only bit 5 set.
pub const BIT5: u8 = 0b0010_0000;
/// Bit pattern with only bit 6 set.
pub const BIT6: u8 = 0b0100_0000;
/// Bit pattern with only bit 7 set.
pub const BIT7: u8 = 0b1000_0000;

/// Multiplier for Hz, essentially another name for 1.
pub const Hz: u32 = 1;
/// Multiplier for kHz, essentially another name for 1'000.
pub const kHz: u32 = 1_000 * Hz;
/// Multiplier for MHz, essentially another name for 1'000'000.
pub const MHz: u32 = 1_000 * kHz;
