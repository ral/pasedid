use nom::bytes::complete::take;
use nom::IResult;

use crate::datamodel::discard_block::EDIDExtensionBlockDiscard;
use crate::definitions::BLOCKSIZE;
use crate::parser::types::{Bytes, Parse};

/// Take and drop a full block size of bytes. That is why we call it a discard block.
fn parse_edid_discard_block(input: Bytes) -> IResult<Bytes, EDIDExtensionBlockDiscard> {
    let (input, _) = take(BLOCKSIZE)(input)?;
    Ok((input, EDIDExtensionBlockDiscard {}))
}

impl Parse for EDIDExtensionBlockDiscard {
    fn parse(input: Bytes) -> IResult<Bytes, Self> {
        parse_edid_discard_block(input)
    }
}
