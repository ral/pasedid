/// Type definitions for parsing.
pub mod types;

/// The parser for complete EDID binary blobs.
pub mod edid;

/// The parser for the EDID base block.
pub mod base_block;
/// The parser for the CEA extension block.
pub mod cea_block;
/// The parser for the DISCARD extension block.
pub mod discard_block;

/// Helper functions for parsing.
pub mod utilities;
