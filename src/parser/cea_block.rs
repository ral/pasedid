use nom::bytes::complete::{tag, take};
use nom::number::complete::be_u8;
use nom::IResult;

use crate::datamodel::cea_block::{EDIDExtensionBlockCEA, BLOCK_TAG};
use crate::datamodel::edid::ExtensionBlockTag;
use crate::definitions::BLOCKSIZE;
use crate::parser::types::{Bytes, Parse};

fn parse_block_tag(input: Bytes) -> IResult<Bytes, ExtensionBlockTag> {
    let (input, _) = tag(&[BLOCK_TAG])(input)?;
    Ok((input, ExtensionBlockTag::CEA))
}

fn parse_version(input: Bytes) -> IResult<Bytes, u8> {
    be_u8(input)
}

// All together

/// Parse a CEA extension block.
fn parse_edid_cea_block(input: Bytes) -> IResult<Bytes, EDIDExtensionBlockCEA> {
    // 1 byte
    let (input, _tag) = parse_block_tag(input)?;

    // 1 byte
    let (input, version) = parse_version(input)?;

    // TODO: Parse CEA internals
    let remaining = BLOCKSIZE - 2;
    let (input, _) = take(remaining)(input)?;

    Ok((input, EDIDExtensionBlockCEA { version }))
}

impl Parse for EDIDExtensionBlockCEA {
    fn parse(input: Bytes) -> IResult<Bytes, Self> {
        parse_edid_cea_block(input)
    }
}
