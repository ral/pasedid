use nom::IResult;

/// Type for byte stream.
pub type Bytes<'a> = &'a [u8];

/// Type for bit stream.
pub type Bits<'a> = (&'a [u8], usize);

/// Parse trait.
pub trait Parse {
    /// Parse a binary blob into a data model structure according to the EDID specification.
    fn parse(input: Bytes) -> IResult<Bytes, Self>
    where
        Self: Sized;
}
