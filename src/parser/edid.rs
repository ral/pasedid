use nom::branch::alt;
use nom::bytes::complete::take;
use nom::combinator::{all_consuming, map, map_parser};
use nom::multi::many0;
use nom::IResult;

use crate::config::Config;
use crate::datamodel::base_block::EDIDBaseBlock;
use crate::datamodel::cea_block::EDIDExtensionBlockCEA;
use crate::datamodel::discard_block::EDIDExtensionBlockDiscard;
use crate::datamodel::edid::{EDIDExtensionBlock, EDID};
use crate::definitions::BLOCKSIZE;
use crate::parser::types::{Bytes, Parse};

fn parse_edid_extension_block(input: Bytes) -> IResult<Bytes, EDIDExtensionBlock> {
    use EDIDExtensionBlock::{CEA, DISCARD};

    // TODO: Handle more block types
    let (input, block) = alt((
        map(EDIDExtensionBlockCEA::parse, CEA),
        map(EDIDExtensionBlockDiscard::parse, |_| DISCARD),
    ))(input)?;

    Ok((input, block))
}

fn parse_edid_extension_blocks(input: Bytes) -> IResult<Bytes, Vec<EDIDExtensionBlock>> {
    // Extension blocks: 128 bytes each
    let (input, extension_blocks) = many0(map_parser(
        take(BLOCKSIZE),
        all_consuming(parse_edid_extension_block),
    ))(input)?;

    Ok((input, extension_blocks))
}

fn parse_edid_helper(input: Bytes, cfg: Config) -> IResult<Bytes, EDID> {
    // Base block: 128 bytes
    let (input, base_block) = map_parser(take(BLOCKSIZE), all_consuming(EDIDBaseBlock::parse))(input)?;

    // Extension blocks: 128 bytes each
    let (input, extension_blocks) = match cfg.handle_extensions {
        true => parse_edid_extension_blocks(input)?,
        false => (input, vec![]),
    };

    Ok((
        input,
        EDID {
            base_block,
            extension_blocks,
        },
    ))
}

/// Parser for a complete EDID binary blob.
pub fn parse_edid(input: Bytes, cfg: Config) -> IResult<Bytes, EDID> {
    match cfg.handle_extensions {
        true => {
            // TODO: Be less strict?
            all_consuming(|input| parse_edid_helper(input, cfg))(input)
        }
        false => {
            // As we do not parse extensions, we might not consume all input.
            parse_edid_helper(input, cfg)
        }
    }
}
