#![allow(dead_code)]

use either::Either;

use nom::IResult;
use nom::ToUsize;

use nom::bits::complete::{bool, take};
use nom::combinator::{map, verify};
use nom::number::complete::le_u8;

use crate::parser::types::Bits;

// Ternary condition operator for brevity.
pub(crate) fn cond<T>(c: bool, a: T, b: T) -> T {
    if c {
        a
    } else {
        b
    }
}

// Test if a single bit inside a byte is set.
pub(crate) fn bit_set(byte: u8, bit: usize) -> bool {
    byte & (1 << bit) > 0
}

// Parse an u8 and check it is not zero
pub(crate) fn pos_le_u8(input: &[u8]) -> IResult<&[u8], u8> {
    verify(le_u8, |x: &u8| *x != 0)(input)
}

pub(crate) fn drop_bits<Count>(i: Bits, n: Count) -> IResult<Bits, ()>
where
    Count: ToUsize,
{
    let (i, _): (Bits, u8) = take(n)(i)?;
    Ok((i, ()))
}

pub(crate) fn drop_bit(i: Bits) -> IResult<Bits, ()> {
    drop_bits(i, 1usize)
}

pub(crate) fn two_bits(i: Bits) -> IResult<Bits, u8> {
    take(2usize)(i)
}

pub(crate) fn take_nibble(i: Bits) -> IResult<Bits, u8> {
    take(4usize)(i)
}

pub(crate) fn value_bitflag_set<Value>(i: Bits, v: Value) -> IResult<Bits, Option<Value>>
where
    Value: Copy,
{
    map(bool, |b| match b {
        false => None,
        true => Some(v),
    })(i)
}

pub(crate) fn value_bitflag_unset<Value>(i: Bits, v: Value) -> IResult<Bits, Option<Value>>
where
    Value: Copy,
{
    map(bool, |b| match b {
        false => Some(v),
        true => None,
    })(i)
}

pub(crate) fn value_bitflag<ValueSet, ValueUnset>(
    i: Bits,
    vs: ValueSet,
    vus: ValueUnset,
) -> IResult<Bits, Either<ValueUnset, ValueSet>>
where
    ValueSet: Copy,
    ValueUnset: Copy,
{
    use Either::{Left, Right};
    map(bool, |b| match b {
        false => Left(vus),
        true => Right(vs),
    })(i)
}
