#![allow(clippy::enum_glob_use)]
#![allow(clippy::wildcard_imports)]

use either::Either;
use hex::encode;

use nom::bits::bits;
use nom::bits::complete::{bool, tag as tag_bits, take as take_bits};
use nom::branch::alt;
use nom::bytes::complete::{tag, take};
use nom::combinator::{map, verify};
use nom::multi::{count, many_m_n};
use nom::number::complete::{le_u16, le_u32, le_u8};
use nom::sequence::{pair, preceded, tuple};
use nom::IResult;

use crate::datamodel::base_block::*;
use crate::definitions::{kHz, Hz, MHz};
use crate::parser::types::{Bits, Bytes, Parse};
use crate::parser::utilities::{
    bit_set, cond, drop_bit, pos_le_u8, take_nibble, two_bits, value_bitflag_set,
};

// SECTION 3.3 Header

fn parse_header(input: Bytes) -> IResult<Bytes, Header> {
    let (input, _) = tag(&MAGIC)(input)?;
    Ok((input, Header::default()))
}

impl Parse for Header {
    fn parse(input: Bytes) -> IResult<Bytes, Self> {
        parse_header(input)
    }
}

// SECTION 3.4 Vendor and product

fn parse_manufacturer_id_helper(i: Bits) -> IResult<Bits, (u8, u8, u8)> {
    preceded(
        drop_bit,
        tuple((take_bits(5usize), take_bits(5usize), take_bits(5usize))),
    )(i)
}

fn parse_manufacturer_id(input: Bytes) -> IResult<Bytes, ManufacturerID> {
    let (input, x) = bits(parse_manufacturer_id_helper)(input)?;
    // TODO: Assert valid char range
    let offset = b'A' - 1;
    let char = |n: u8| (n + offset) as char;
    let (c1, c2, c3) = (char(x.0), char(x.1), char(x.2));
    Ok((input, ManufacturerID(c1, c2, c3)))
}

#[allow(clippy::match_same_arms)]
fn parse_manufacture_week(input: Bytes) -> IResult<Bytes, u8> {
    map(verify(le_u8, |x| *x != 0xff), |v| match v {
        0x00 => 0,              // Unspecified week
        0x01..=0x36 => v,       // Valid week number
        0x37..=0xfe => 0,       // Reserved, do not use
        0xff => unreachable!(), // Model year flag
    })(input)
}

fn parse_manufacture_year(input: Bytes) -> IResult<Bytes, u16> {
    map(le_u8, |v| 1990 + u16::from(v))(input)
}

fn parse_manufacture_week_year(input: Bytes) -> IResult<Bytes, ManufactureDate> {
    let (input, week) = parse_manufacture_week(input)?;
    let (input, year) = parse_manufacture_year(input)?;
    Ok((
        input,
        ManufactureDate::ManufactureWeekYear(ManufactureWeekYear { week, year }),
    ))
}

fn parse_manufacture_model_year(input: Bytes) -> IResult<Bytes, ManufactureDate> {
    let (input, _) = tag([0xff])(input)?;
    let (input, year) = parse_manufacture_year(input)?;
    Ok((input, ManufactureDate::ManufactureModelYear(year)))
}

fn parse_manufacture_date(input: Bytes) -> IResult<Bytes, ManufactureDate> {
    alt((parse_manufacture_week_year, parse_manufacture_model_year))(input)
}

fn parse_vendor_product_identification(input: Bytes) -> IResult<Bytes, VendorProductIdentification> {
    let (input, manufacturer_id) = parse_manufacturer_id(input)?;
    let (input, product_code) = le_u16(input)?;
    let (input, serial_number) = le_u32(input)?;
    let (input, manufacture_date) = parse_manufacture_date(input)?;
    Ok((
        input,
        VendorProductIdentification {
            manufacturer_id,
            product_code,
            serial_number,
            manufacture_date,
        },
    ))
}

impl Parse for VendorProductIdentification {
    fn parse(input: Bytes) -> IResult<Bytes, Self> {
        parse_vendor_product_identification(input)
    }
}

// SECTION 3.5 Structure version and revision

impl Parse for Version {
    fn parse(input: Bytes) -> IResult<Bytes, Self> {
        // TODO: Err out if edid version is not 1.4 as parse result might be wrong?
        map(pair(le_u8, le_u8), |(version, revision)| Version {
            version,
            revision,
        })(input)
    }
}

// SECTION 3.6 Basic display parameters and features

fn parse_signal_level(i: Bits) -> IResult<Bits, SignalLevel> {
    map(take_bits(2usize), |sl| match sl {
        0b00 => SignalLevel {
            video: 0.700,
            sync: 0.300,
        },
        0b01 => SignalLevel {
            video: 0.714,
            sync: 0.286,
        },
        0b10 => SignalLevel {
            video: 1.000,
            sync: 0.400,
        },
        0b11 => SignalLevel {
            video: 0.700,
            sync: 0.000,
        },
        _ => unreachable!(),
    })(i)
}

fn parse_synchronization_support(i: Bits) -> IResult<Bits, SynchronizationSupport> {
    let (i, separate_sync) = bool(i)?;
    let (i, composite_sync) = bool(i)?;
    let (i, sync_on_green) = bool(i)?;
    Ok((
        i,
        SynchronizationSupport {
            separate_sync,
            composite_sync,
            sync_on_green,
        },
    ))
}

fn parse_video_input_definition_analog_helper(i: Bits) -> IResult<Bits, VideoInputDefinition> {
    let (i, _) = tag_bits(0b0, 1usize)(i)?;
    let (i, signal_level) = parse_signal_level(i)?;
    let (i, video_setup) = bool(i)?;
    let (i, sync) = parse_synchronization_support(i)?;
    let (i, serration) = bool(i)?;
    Ok((
        i,
        VideoInputDefinition::Analog {
            signal_level,
            video_setup,
            sync,
            serration,
        },
    ))
}

fn parse_color_bit_depth(i: Bits) -> IResult<Bits, ColorBitDepth> {
    use ColorBitDepth::*;
    map(take_bits(3usize), |cd| match cd {
        0b000 => Undefined,
        0b001 => PrimaryColorBits6,
        0b010 => PrimaryColorBits8,
        0b011 => PrimaryColorBits10,
        0b100 => PrimaryColorBits12,
        0b101 => PrimaryColorBits14,
        0b110 => PrimaryColorBits16,
        _ => unreachable!(), // Reserved
    })(i)
}

fn parse_digital_video_interface_standard(i: Bits) -> IResult<Bits, DigitalVideoInterfaceStandard> {
    use DigitalVideoInterfaceStandard::*;
    map(take_bits(4usize), |vi| match vi {
        0b0000 => Undefined,
        0b0001 => DVI,
        0b0010 => HDMIA,
        0b0011 => HDMIB,
        0b0100 => MDDI,
        0b0101 => DP,
        _ => unreachable!(), // Reserved
    })(i)
}

fn parse_video_input_definition_digital_helper(i: Bits) -> IResult<Bits, VideoInputDefinition> {
    let (i, _) = tag_bits(0b1, 1usize)(i)?;
    let (i, color_depth) = parse_color_bit_depth(i)?;
    let (i, video_interface) = parse_digital_video_interface_standard(i)?;
    Ok((
        i,
        VideoInputDefinition::Digital {
            color_depth,
            video_interface,
        },
    ))
}

fn parse_video_input_definition(input: Bytes) -> IResult<Bytes, VideoInputDefinition> {
    alt((
        bits(parse_video_input_definition_analog_helper),
        bits(parse_video_input_definition_digital_helper),
    ))(input)
}

fn parse_screen_size_helper(input: Bytes) -> IResult<Bytes, ScreenSizeOrAspectRatio> {
    map(tuple((le_u8, le_u8)), |(v1, v2)| {
        ScreenSizeOrAspectRatio::ScreenSize(ScreenSize {
            horizontal: v1,
            vertical: v2,
        })
    })(input)
}

fn parse_aspect_ratio_p_helper(input: Bytes) -> IResult<Bytes, ScreenSizeOrAspectRatio> {
    map(tuple((tag([0x00]), le_u8)), |(_, v2)| {
        ScreenSizeOrAspectRatio::AspectRatio(NumericAspectRatio {
            orientation: ScreenOrientation::PORTRAIT,
            ratio: 100.0 / (f32::from(v2) + 99.0),
        })
    })(input)
}

fn parse_aspect_ratio_l_helper(input: Bytes) -> IResult<Bytes, ScreenSizeOrAspectRatio> {
    map(tuple((le_u8, tag([0x00]))), |(v1, _)| {
        ScreenSizeOrAspectRatio::AspectRatio(NumericAspectRatio {
            orientation: ScreenOrientation::LANDSCAPE,
            ratio: (f32::from(v1) + 99.0) / 100.0,
        })
    })(input)
}

fn parse_screen_size_or_aspect_ratio_undefined_helper(
    input: Bytes,
) -> IResult<Bytes, ScreenSizeOrAspectRatio> {
    map(tag([0x00, 0x00]), |_| ScreenSizeOrAspectRatio::Unknown)(input)
}

fn parse_screen_size_or_aspect_ratio(input: Bytes) -> IResult<Bytes, ScreenSizeOrAspectRatio> {
    alt((
        parse_screen_size_or_aspect_ratio_undefined_helper,
        parse_aspect_ratio_l_helper,
        parse_aspect_ratio_p_helper,
        parse_screen_size_helper,
    ))(input)
}

fn parse_gamma(input: Bytes) -> IResult<Bytes, TransferCharacteristic> {
    use TransferCharacteristic::*;
    map(pos_le_u8, |gv| match gv {
        0x00 => unreachable!(),                      // Illegal value
        0xff => SeeOther,                            // Not defined here
        _ => Gamma((f32::from(gv) + 100.0) / 100.0), // Regular value
    })(input)
}

fn parse_display_power_management(i: Bits) -> IResult<Bits, DisplayPowerManagement> {
    let (i, standby_supported) = bool(i)?;
    let (i, suspend_supported) = bool(i)?;
    let (i, active_off_supported) = bool(i)?;
    Ok((
        i,
        DisplayPowerManagement {
            standby_supported,
            suspend_supported,
            active_off_supported,
        },
    ))
}

fn parse_display_color_type(i: Bits) -> IResult<Bits, DisplayColorType> {
    use DisplayColorType::*;
    map(take_bits(2usize), |dct| match dct {
        0b00 => Grayscale,
        0b01 => RGBColor,
        0b10 => OtherColor,
        0b11 => Undefined,
        _ => unreachable!(),
    })(i)
}

fn parse_color_encoding_format(i: Bits) -> IResult<Bits, ColorEncodingFormat> {
    use ColorEncodingFormat::*;
    map(take_bits(2usize), |cef| match cef {
        0b00 => RGB444,
        0b01 => RGB444YCrCb444,
        0b10 => RGB444YCrCb422,
        0b11 => RGB444YCrCb444YCrCb422,
        _ => unreachable!(),
    })(i)
}

fn parse_display_color_support<'l>(
    i: Bits<'l>,
    vid: &VideoInputDefinition,
) -> IResult<Bits<'l>, DisplayColorTypeOrColorEncodingFormat> {
    use DisplayColorTypeOrColorEncodingFormat::*;
    match vid {
        VideoInputDefinition::Analog {
            signal_level: _,
            video_setup: _,
            sync: _,
            serration: _,
        } => map(parse_display_color_type, DisplayColorType)(i),
        VideoInputDefinition::Digital {
            color_depth: _,
            video_interface: _,
        } => map(parse_color_encoding_format, ColorEncodingFormat)(i),
    }
}

fn parse_feature_support_flags(i: Bits) -> IResult<Bits, FeatureSupportFlags> {
    let (i, srgb_default) = bool(i)?;
    let (i, preferred_timing_mode) = bool(i)?;
    let (i, continuous_frequency) = bool(i)?;
    Ok((
        i,
        FeatureSupportFlags {
            srgb_default,
            preferred_timing_mode,
            continuous_frequency,
        },
    ))
}

fn parse_display_features<'l>(i: Bits<'l>, vid: &VideoInputDefinition) -> IResult<Bits<'l>, DisplayFeatures> {
    let (i, dpm_support) = parse_display_power_management(i)?;
    let (i, color_support) = parse_display_color_support(i, vid)?;
    let (i, feature_support_flags) = parse_feature_support_flags(i)?;
    Ok((
        i,
        DisplayFeatures {
            dpm_support,
            color_support,
            feature_support_flags,
        },
    ))
}

fn parse_display_parameters(input: Bytes) -> IResult<Bytes, DisplayParameters> {
    let (input, video_input_definition) = parse_video_input_definition(input)?;
    let (input, screen_size) = parse_screen_size_or_aspect_ratio(input)?;
    let (input, gamma) = parse_gamma(input)?;
    let (input, features) = bits(|x| parse_display_features(x, &video_input_definition))(input)?;
    Ok((
        input,
        DisplayParameters {
            video_input_definition,
            screen_size,
            gamma,
            features,
        },
    ))
}

impl Parse for DisplayParameters {
    fn parse(input: Bytes) -> IResult<Bytes, Self> {
        parse_display_parameters(input)
    }
}

// SECTION 3.7 Display chromaticity coordinates

fn parse_chromaticity_coordinates_helper(i: Bits) -> IResult<Bits, (u8, u8, u8, u8)> {
    tuple((two_bits, two_bits, two_bits, two_bits))(i)
}

#[allow(clippy::similar_names)]
fn parse_chromaticity_coordinates(input: Bytes) -> IResult<Bytes, ChromaticityCoordinates> {
    // Combine 8 high and 2 low bits into a 10 bit number.
    let number = |high: u8, low: u8| u16::from(high) << 2 | u16::from(low);
    // Convert 10 bit binary fraction representation to float.
    let float = |value: u16| f32::from(value) / 1024.0;

    let (input, (rx_l, ry_l, gx_l, gy_l)) = bits(parse_chromaticity_coordinates_helper)(input)?;
    let (input, (bx_l, by_l, wx_l, wy_l)) = bits(parse_chromaticity_coordinates_helper)(input)?;
    let (input, (rx_h, ry_h)) = tuple((le_u8, le_u8))(input)?;
    let (input, (gx_h, gy_h)) = tuple((le_u8, le_u8))(input)?;
    let (input, (bx_h, by_h)) = tuple((le_u8, le_u8))(input)?;
    let (input, (wx_h, wy_h)) = tuple((le_u8, le_u8))(input)?;

    Ok((
        input,
        ChromaticityCoordinates {
            red: CIECoordinates {
                x: float(number(rx_h, rx_l)),
                y: float(number(ry_h, ry_l)),
            },
            green: CIECoordinates {
                x: float(number(gx_h, gx_l)),
                y: float(number(gy_h, gy_l)),
            },
            blue: CIECoordinates {
                x: float(number(bx_h, bx_l)),
                y: float(number(by_h, by_l)),
            },
            white: CIECoordinates {
                x: float(number(wx_h, wx_l)),
                y: float(number(wy_h, wy_l)),
            },
        },
    ))
}

impl Parse for ChromaticityCoordinates {
    fn parse(input: Bytes) -> IResult<Bytes, Self> {
        parse_chromaticity_coordinates(input)
    }
}

// SECTION 3.8 Established timings I and II

fn parse_established_timing_helper(i: Bits) -> IResult<Bits, EstablishedTimings> {
    use EstablishedTiming::*;
    // Byte 23
    let (i, t01) = value_bitflag_set(i, H720V400F70)?;
    let (i, t02) = value_bitflag_set(i, H720V400F88)?;
    let (i, t03) = value_bitflag_set(i, H640V480F60)?;
    let (i, t04) = value_bitflag_set(i, H640V480F67)?;
    let (i, t05) = value_bitflag_set(i, H640V480F72)?;
    let (i, t06) = value_bitflag_set(i, H640V480F75)?;
    let (i, t07) = value_bitflag_set(i, H800V600F56)?;
    let (i, t08) = value_bitflag_set(i, H800V600F60)?;
    // Byte 24
    let (i, t11) = value_bitflag_set(i, H800V600F72)?;
    let (i, t12) = value_bitflag_set(i, H800V600F75)?;
    let (i, t13) = value_bitflag_set(i, H832V624F75)?;
    let (i, t14) = value_bitflag_set(i, H1024V768F87)?;
    let (i, t15) = value_bitflag_set(i, H1024V768F60)?;
    let (i, t16) = value_bitflag_set(i, H1024V768F70)?;
    let (i, t17) = value_bitflag_set(i, H1024V768F75)?;
    let (i, t18) = value_bitflag_set(i, H1280V1024F75)?;
    // Byte 25
    let (i, t21) = value_bitflag_set(i, H1152V870F75)?;
    // Reserved manufacturer specific bits
    let (i, _) = count(drop_bit, 7)(i)?;

    #[rustfmt::skip]
    let timings = vec![
        t01, t02, t03, t04, t05, t06, t07, t08,
        t11, t12, t13, t14, t15, t16, t17, t18,
        t21,
    ];
    let timings = EstablishedTimings {
        established_timings: timings.into_iter().flatten().collect(),
    };

    Ok((i, timings))
}

fn parse_established_timing(input: Bytes) -> IResult<Bytes, EstablishedTimings> {
    bits(parse_established_timing_helper)(input)
}

impl Parse for EstablishedTimings {
    fn parse(input: Bytes) -> IResult<Bytes, Self> {
        parse_established_timing(input)
    }
}

// SECTION 3.9 Standard timings

fn parse_standard_timing_aspect_ratio_helper(i: Bits) -> IResult<Bits, AspectRatioSymbol> {
    use AspectRatioSymbol::*;
    map(take_bits(2usize), |cef| match cef {
        0b00 => AR16TO10,
        0b01 => AR4TO3,
        0b10 => AR5TO4,
        0b11 => AR16TO9,
        _ => unreachable!(),
    })(i)
}

fn parse_standard_timing_refresh_rate_helper(i: Bits) -> IResult<Bits, u8> {
    map(take_bits(6usize), |v: u8| v + 60)(i)
}

fn parse_standard_timing_helper(i: Bits) -> IResult<Bits, (AspectRatioSymbol, u8)> {
    pair(
        parse_standard_timing_aspect_ratio_helper,
        parse_standard_timing_refresh_rate_helper,
    )(i)
}

fn parse_standard_timing(input: Bytes) -> IResult<Bytes, StandardTiming> {
    let (input, horizontal_resolution) = map(pos_le_u8, |v: u8| (u16::from(v) + 31) * 8)(input)?;
    let (input, (aspect_ratio, refresh_rate)) = bits(parse_standard_timing_helper)(input)?;
    Ok((
        input,
        StandardTiming {
            horizontal_resolution,
            aspect_ratio,
            refresh_rate,
        },
    ))
}

fn parse_standard_timing_optional(input: Bytes) -> IResult<Bytes, Option<StandardTiming>> {
    alt((map(tag([0x01, 0x01]), |_| None), map(parse_standard_timing, Some)))(input)
}

fn parse_standard_timings(input: Bytes) -> IResult<Bytes, StandardTimings> {
    let (input, timings) = many_m_n(8, 8, parse_standard_timing_optional)(input)?;
    let standard_timings = timings.into_iter().flatten().collect();
    Ok((input, StandardTimings(standard_timings)))
}

impl Parse for StandardTimings {
    fn parse(input: Bytes) -> IResult<Bytes, Self> {
        parse_standard_timings(input)
    }
}

// SECTION 3.10 18 Byte descriptors

// SECTION 3.10.2 Detailed timing descriptor

fn parse_detailed_timing_helper(
    input: Bytes,
) -> IResult<Bytes, (SignalInterface, StereoViewingType, SyncSignalDefinition)> {
    let (input, byte) = le_u8(input)?;

    // Signal interface type
    // Bit 7
    let signal_interface = match bit_set(byte, 7) {
        true => SignalInterface::Interlaced,
        false => SignalInterface::NonInterlaced,
    };

    // Stereo viewing support
    // Bits 6, 5, 0
    let stereo_viewing = match (bit_set(byte, 6), bit_set(byte, 5), bit_set(byte, 0)) {
        (false, false, false) => StereoViewingType::NoStereo,
        (false, false, true) => StereoViewingType::DoNotCare,
        (false, true, false) => StereoViewingType::SequentialRightSync,
        (true, false, false) => StereoViewingType::SequentialLeftSync,
        (false, true, true) => StereoViewingType::InterleavedLinesRightEven,
        (true, false, true) => StereoViewingType::InterleavedLinesLeftEven,
        (true, true, false) => StereoViewingType::Interleaved4Way,
        (true, true, true) => StereoViewingType::SideBySide,
    };

    // Sync signal definitions
    // Bits 4, 3, 2, 1
    let sync_signal_type = match (bit_set(byte, 4), bit_set(byte, 3)) {
        (false, false | true) => SyncSignalDefinition::AnalogComposite {
            sync: cond(
                bit_set(byte, 3),
                SyncSignal::BipolarAnalogComposite,
                SyncSignal::AnalogComposite,
            ),
            serrations: bit_set(byte, 2),
            line: cond(bit_set(byte, 1), SyncLine::RGB, SyncLine::Green),
        },
        (true, false) => SyncSignalDefinition::DigitalComposite {
            serrations: bit_set(byte, 2),
            horizontal: cond(bit_set(byte, 1), SyncPolarity::Positive, SyncPolarity::Negative),
        },
        (true, true) => SyncSignalDefinition::DigitalSeparate {
            vertical: cond(bit_set(byte, 2), SyncPolarity::Positive, SyncPolarity::Negative),
            horizontal: cond(bit_set(byte, 1), SyncPolarity::Positive, SyncPolarity::Negative),
        },
    };

    Ok((input, (signal_interface, stereo_viewing, sync_signal_type)))
}

#[allow(clippy::similar_names)]
fn parse_detailed_timing(input: Bytes) -> IResult<Bytes, DetailedTiming> {
    // Combine 4 high and 8 low bits into a 12 bit number.
    let number12b = |high: u8, low: u8| (u16::from(high) << 8) | u16::from(low);
    // Combine 2 high and 8 low bits into a 10 bit number.
    let number10b = |high: u8, low: u8| (u16::from(high) << 8) | u16::from(low);
    // Combine 2 high and 4 low bits into a 6 bit number.
    let number6b = |high: u8, low: u8| (high << 4) | low;

    // Bytes 0, 1
    let (input, pixel_clock) = map(verify(le_u16, |x: &u16| *x != 0), |x| u32::from(x) * 10 * kHz)(input)?;

    // Bytes 2, ..., 7
    let (input, (hv_l, hb_l)) = tuple((le_u8, le_u8))(input)?;
    let (input, (hv_h, hb_h)) = bits(tuple((take_nibble, take_nibble)))(input)?;
    let (input, (vv_l, vb_l)) = tuple((le_u8, le_u8))(input)?;
    let (input, (vv_h, vb_h)) = bits(tuple((take_nibble, take_nibble)))(input)?;
    let addressable_video = AddressableVideo {
        horizontal: number12b(hv_h, hv_l),
        vertical: number12b(vv_h, vv_l),
    };
    let blanking = Blanking {
        horizontal: number12b(hb_h, hb_l),
        vertical: number12b(vb_h, vb_l),
    };

    // Bytes 8, ..., 11
    let (input, (hp_l, hs_l)) = tuple((le_u8, le_u8))(input)?;
    let (input, (vp_l, vs_l)) = bits(tuple((take_nibble, take_nibble)))(input)?;
    let (input, (hp_h, hs_h, vp_h, vs_h)) = bits(tuple((two_bits, two_bits, two_bits, two_bits)))(input)?;
    let front_porch = FrontPorch {
        horizontal: number10b(hp_h, hp_l),
        vertical: number6b(vp_h, vp_l),
    };
    let sync_pulse_width = SyncPulseWidth {
        horizontal: number10b(hs_h, hs_l),
        vertical: number6b(vs_h, vs_l),
    };

    // Bytes 12, 13, 14
    let (input, (hi_l, vi_l)) = tuple((le_u8, le_u8))(input)?;
    let (input, (hi_h, vi_h)) = bits(tuple((take_nibble, take_nibble)))(input)?;
    let video_image_size = VideoImageSize {
        width: number12b(hi_h, hi_l),
        height: number12b(vi_h, vi_l),
    };

    // Bytes 15, 16
    // pub border_size: BorderSize,
    let (input, (hb, vb)) = tuple((le_u8, le_u8))(input)?;
    let border_size = BorderSize { top: vb, left: hb };

    // Byte 17
    let (input, (signal_interface, stereo_viewing, sync_signal_type)) = parse_detailed_timing_helper(input)?;

    Ok((
        input,
        DetailedTiming {
            pixel_clock,
            addressable_video,
            blanking,
            front_porch,
            sync_pulse_width,
            video_image_size,
            border_size,
            signal_interface,
            stereo_viewing,
            sync_signal_type,
        },
    ))
}

// SECTION 3.10.3 Display descriptor definitions

// Display product serial number.
fn parse_product_serial_number(input: Bytes) -> IResult<Bytes, DisplayDescriptor> {
    // Bytes 0, 1
    let (input, _dd) = tag([0x00, 0x00])(input)?;
    // Byte 2
    let (input, _reserved) = tag([0x00])(input)?;
    // Byte 3
    let (input, _descriptor_tag) = tag([0xff])(input)?;
    // Byte 4
    let (input, _reserved) = tag([0x00])(input)?;
    // Bytes 5, ..., 17
    let (input, string) = take(13usize)(input)?;

    let serial_number_raw = String::from_utf8(string.to_vec()).unwrap();
    let serial_number = match serial_number_raw.split_once('\n') {
        Some((prefix, _)) => String::from(prefix),
        None => serial_number_raw,
    };

    Ok((
        input,
        DisplayDescriptor::ProductSerialNumber(ProductSerialNumber { serial_number }),
    ))
}

// Alphanumeric data string.
fn parse_alphanumeric_data_string(input: Bytes) -> IResult<Bytes, DisplayDescriptor> {
    // Bytes 0, 1
    let (input, _dd) = tag([0x00, 0x00])(input)?;
    // Byte 2
    let (input, _reserved) = tag([0x00])(input)?;
    // Byte 3
    let (input, _descriptor_tag) = tag([0xfe])(input)?;
    // Byte 4
    let (input, _reserved) = tag([0x00])(input)?;
    // Bytes 5, ..., 17
    let (input, string) = take(13usize)(input)?;

    let string_raw = String::from_utf8(string.to_vec()).unwrap();
    let string = match string_raw.split_once('\n') {
        Some((prefix, _)) => String::from(prefix),
        None => string_raw,
    };

    Ok((
        input,
        DisplayDescriptor::AlphanumericDataString(AlphanumericDataString { string }),
    ))
}

// Display range limits.
fn parse_range_limit_offset(i: Bits) -> IResult<Bits, RangeLimitOffset> {
    use RangeLimitOffset::*;
    map(take_bits(2usize), |vi| match vi {
        0b00 => None,
        // 0b01 => // Reserved
        0b10 => Max,
        0b11 => MinMax,
        _ => unreachable!(), // Reserved
    })(i)
}

fn parse_range_limit_offsets_helper(i: Bits) -> IResult<Bits, RangeLimitOffsets> {
    let (i, _reserved) = tag_bits(0b0000, 4usize)(i)?;
    let (i, vertical_offset) = parse_range_limit_offset(i)?;
    let (i, horizontal_offset) = parse_range_limit_offset(i)?;
    Ok((
        i,
        RangeLimitOffsets {
            vertical: vertical_offset,
            horizontal: horizontal_offset,
        },
    ))
}

fn parse_range_limit_offsets(input: Bytes) -> IResult<Bytes, RangeLimitOffsets> {
    bits(parse_range_limit_offsets_helper)(input)
}

fn offset_range_limits(off: RangeLimitOffset, (mi, ma): (u8, u8), mult: u32) -> MinMaxRange<u32> {
    use RangeLimitOffset::*;
    let mi = u32::from(mi);
    let ma = u32::from(ma);
    match off {
        None => MinMaxRange {
            min: mi * mult,
            max: ma * mult,
        },
        Max => MinMaxRange {
            min: mi * mult,
            max: (ma + 255) * mult,
        },
        MinMax => MinMaxRange {
            min: (mi + 255) * mult,
            max: (ma + 255) * mult,
        },
    }
}

fn parse_timing_support_flags(input: Bytes) -> IResult<Bytes, TimingSupportFlags> {
    use TimingSupportFlags::*;
    map(le_u8, |vi| match vi {
        0x00 => DefaultGTF,
        0x01 => RangeLimitsOnly,
        0x02 => SecondaryGTF,
        // 0x03 => // Reserved
        0x04 => CVTSupport,
        _ => unreachable!(), // Reserved
    })(input)
}

fn parse_gtf_curve_definition(input: Bytes) -> IResult<Bytes, GTFCurveDefinition> {
    // Byte 11
    let (input, _reserved) = tag([0x00, 0x00])(input)?;
    // Byte 12
    let (input, frequency) = map(le_u8, |x| u32::from(x) * 2 * kHz)(input)?;
    // Byte 13
    let (input, c) = map(le_u8, |x| x / 2)(input)?;
    // Bytes 14, 15
    let (input, m) = le_u16(input)?;
    // Byte 16
    let (input, k) = le_u8(input)?;
    // Byte 17
    let (input, j) = map(le_u8, |x| x / 2)(input)?;

    Ok((
        input,
        GTFCurveDefinition {
            break_frequency: frequency,
            c,
            m,
            k,
            j,
        },
    ))
}

fn parse_cvt_definition_helper(i: Bits) -> IResult<Bits, (u8, u16)> {
    tuple((take_bits(6usize), take_bits(10usize)))(i)
}

fn parse_cvt_supported_aspect_ratio(i: Bits) -> IResult<Bits, Vec<CVTAspectRatio>> {
    use CVTAspectRatio::*;
    // Byte 14
    let (i, ar1) = value_bitflag_set(i, AR4TO3)?;
    let (i, ar2) = value_bitflag_set(i, AR16TO9)?;
    let (i, ar3) = value_bitflag_set(i, AR16TO10)?;
    let (i, ar4) = value_bitflag_set(i, AR5TO4)?;
    let (i, ar5) = value_bitflag_set(i, AR15TO9)?;
    let (i, _reserved) = tag_bits(0b000, 3usize)(i)?;

    let ratios = vec![ar1, ar2, ar3, ar4, ar5];
    let ratios = ratios.into_iter().flatten().collect();

    Ok((i, ratios))
}

fn parse_cvt_aspect_ratio_and_blanking(i: Bits) -> IResult<Bits, (CVTAspectRatio, CVTBlankingSupport)> {
    use CVTAspectRatio::*;
    // Byte 15: Bits 7, 6, 5
    let (i, par) = map(take_bits(3usize), |par| match par {
        0b000 => AR4TO3,
        0b001 => AR16TO9,
        0b010 => AR16TO10,
        0b011 => AR5TO4,
        0b100 => AR15TO9,
        _ => unreachable!(), // Reserved
    })(i)?;
    // Byte 15: Bits 4, 3
    let (i, blanking) = map(pair(bool, bool), |(standard, reduced)| CVTBlankingSupport {
        standard,
        reduced,
    })(i)?;
    // Byte 15: Bits 2, 1, 0
    // Reserved
    let (i, _reserved) = tag_bits(0b000, 3usize)(i)?;

    Ok((i, (par, blanking)))
}

fn parse_cvt_display_scaling(i: Bits) -> IResult<Bits, Vec<CVTDisplayScaling>> {
    use CVTDisplayScaling::*;
    // Byte 16
    let (i, s1) = value_bitflag_set(i, HorizontalShrink)?;
    let (i, s2) = value_bitflag_set(i, HorizontalStretch)?;
    let (i, s3) = value_bitflag_set(i, VerticalShrink)?;
    let (i, s4) = value_bitflag_set(i, VerticalStretch)?;
    let (i, _reserved) = tag_bits(0b0000, 4usize)(i)?;

    let scalings = vec![s1, s2, s3, s4];
    let scalings = scalings.into_iter().flatten().collect();

    Ok((i, scalings))
}

fn parse_cvt_definition(input: Bytes) -> IResult<Bytes, CVTDefinition> {
    use ActivePixels::*;
    // Byte 11
    let (input, standard_version) = bits(tuple((take_nibble, take_nibble)))(input)?;
    // Bytes 12, 13
    let (input, (prec, ap)) = bits(parse_cvt_definition_helper)(input)?;
    let active_pixels_per_line = cond(ap.trailing_zeros() >= 8, Unlimited, Maximum(8 * ap));
    let pixel_clock_precision = u32::from(prec) * MHz / 4;
    // Byte 14
    let (input, supported_aspect_ratios) = bits(parse_cvt_supported_aspect_ratio)(input)?;
    // Byte 15
    let (input, (preferred_aspect_ratio, blanking_support)) =
        bits(parse_cvt_aspect_ratio_and_blanking)(input)?;
    // Byte 16
    let (input, display_scaling_type) = bits(parse_cvt_display_scaling)(input)?;
    // Byte 17
    let (input, preferred_vertical_refresh_rate) = pos_le_u8(input)?;

    Ok((
        input,
        CVTDefinition {
            standard_version,
            pixel_clock_precision,
            active_pixels_per_line,
            supported_aspect_ratios,
            preferred_aspect_ratio,
            blanking_support,
            display_scaling_type,
            preferred_vertical_refresh_rate,
        },
    ))
}

fn parse_video_timing_data_padding(input: Bytes) -> IResult<Bytes, VideoTimingData> {
    // TODO: Be more or less restrictive here?

    // Parse and check for proper padding values
    const PADDING: [u8; 7] = [0x0a, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20];
    let (input, _padding) = tag(PADDING)(input)?;

    // Just discard padding bytes
    // let (input, _padding) = take(7usize)(input)?;

    Ok((input, VideoTimingData::None))
}

fn parse_video_timing_data(input: Bytes, flags: TimingSupportFlags) -> IResult<Bytes, VideoTimingData> {
    use TimingSupportFlags::*;
    use VideoTimingData::*;
    let (input, timing_data) = match flags {
        DefaultGTF | RangeLimitsOnly => parse_video_timing_data_padding(input)?,
        SecondaryGTF => map(parse_gtf_curve_definition, GTF)(input)?,
        CVTSupport => map(parse_cvt_definition, CVT)(input)?,
    };
    Ok((input, timing_data))
}

#[allow(clippy::similar_names)]
fn parse_range_limits(input: Bytes) -> IResult<Bytes, DisplayDescriptor> {
    // Bytes 0, 1
    let (input, _dd) = tag([0x00, 0x00])(input)?;
    // Byte 2
    let (input, _reserved) = tag([0x00])(input)?;
    // Byte 3
    let (input, _descriptor_tag) = tag([0xfd])(input)?;
    // Byte 4
    let (input, range_limits_offsets) = parse_range_limit_offsets(input)?;
    // Bytes 5, ..., 8
    let (input, (mivr, mavr, mihr, mahr)) = tuple((pos_le_u8, pos_le_u8, pos_le_u8, pos_le_u8))(input)?;
    let vertical_rate = offset_range_limits(range_limits_offsets.vertical, (mivr, mavr), Hz);
    let horizontal_rate = offset_range_limits(range_limits_offsets.horizontal, (mihr, mahr), kHz);
    // Byte 9
    let (input, max_pixel_clock) = map(pos_le_u8, |x| u32::from(x) * 10 * MHz)(input)?;
    // Byte 10
    let (input, timing_support_flags) = parse_timing_support_flags(input)?;
    // Bytes 11, ..., 17
    let (input, video_timing_data) = parse_video_timing_data(input, timing_support_flags)?;

    Ok((
        input,
        DisplayDescriptor::RangeLimits(RangeLimits {
            range_limits_offsets,
            vertical_rate,
            horizontal_rate,
            max_pixel_clock,
            timing_support_flags,
            video_timing_data,
        }),
    ))
}

// Display product name.
fn parse_product_name(input: Bytes) -> IResult<Bytes, DisplayDescriptor> {
    // Bytes 0, 1
    let (input, _dd) = tag([0x00, 0x00])(input)?;
    // Byte 2
    let (input, _reserved) = tag([0x00])(input)?;
    // Byte 3
    let (input, _descriptor_tag) = tag([0xfc])(input)?;
    // Byte 4
    let (input, _reserved) = tag([0x00])(input)?;
    // Bytes 5, ..., 17
    let (input, string) = take(13usize)(input)?;

    let string_raw = String::from_utf8(string.to_vec()).unwrap();
    let name = match string_raw.split_once('\n') {
        Some((prefix, _)) => String::from(prefix),
        None => string_raw,
    };

    Ok((input, DisplayDescriptor::ProductName(ProductName { name })))
}

// Color point data.
#[allow(clippy::similar_names)]
fn parse_whitepoint_coordinates(input: Bytes) -> IResult<Bytes, CIECoordinates> {
    // Combine 8 high and 2 low bits into a 10 bit number.
    let number = |high: u8, low: u8| u16::from(high) << 2 | u16::from(low);
    // Convert 10 bit binary fraction representation to float.
    let float = |value: u16| f32::from(value) / 1024.0;

    // Byte 6
    let (input, (_, wx_l, wy_l)) = bits(tuple((take_nibble, two_bits, two_bits)))(input)?;
    // Bytes 7, 8
    let (input, (wx_h, wy_h)) = tuple((le_u8, le_u8))(input)?;

    Ok((
        input,
        CIECoordinates {
            x: float(number(wx_h, wx_l)),
            y: float(number(wy_h, wy_l)),
        },
    ))
}

fn parse_white_point_datum(input: Bytes) -> IResult<Bytes, Option<WhitePointDatum>> {
    // Byte 5
    let (input, index) = pos_le_u8(input)?;
    // Bytes 6, 7, 8
    let (input, white_point) = parse_whitepoint_coordinates(input)?;
    // Byte 9
    let (input, gamma) = parse_gamma(input)?;

    Ok((
        input,
        Some(WhitePointDatum {
            index,
            white_point,
            gamma,
        }),
    ))
}

fn parse_white_point_padding(input: Bytes) -> IResult<Bytes, Option<WhitePointDatum>> {
    // Byte 10
    let (input, _empty) = tag([0x00])(input)?;
    // Bytes 11, ..., 14
    let (input, _padding) = tag([0x00, 0x00, 0x00, 0x00])(input)?;
    // Padding, always return None
    Ok((input, None))
}

fn parse_color_point_data(input: Bytes) -> IResult<Bytes, DisplayDescriptor> {
    // Bytes 0, 1
    let (input, _dd) = tag([0x00, 0x00])(input)?;
    // Byte 2
    let (input, _reserved) = tag([0x00])(input)?;
    // Byte 3
    let (input, _descriptor_tag) = tag([0xfb])(input)?;
    // Byte 4
    let (input, _reserved) = tag([0x00])(input)?;
    // Bytes 5, ..., 9
    let (input, wp1) = parse_white_point_datum(input)?;
    // Bytes 10, ..., 14
    let (input, wp2) = alt((parse_white_point_datum, parse_white_point_padding))(input)?;
    // Bytes 15, 16, 17
    let (input, _padding) = tag([0x0a, 0x20, 0x20])(input)?;

    let white_points = vec![wp1, wp2];
    let white_points = white_points.into_iter().flatten().collect();

    // TODO: Assert indices are different?

    Ok((
        input,
        DisplayDescriptor::ColorPointData(ColorPointData { white_points }),
    ))
}

// Standard timing identifiers.
fn parse_standard_timing_identifiers(input: Bytes) -> IResult<Bytes, DisplayDescriptor> {
    // Bytes 0, 1
    let (input, _dd) = tag([0x00, 0x00])(input)?;
    // Byte 2
    let (input, _reserved) = tag([0x00])(input)?;
    // Byte 3
    let (input, _descriptor_tag) = tag([0xfa])(input)?;
    // Byte 4
    let (input, _reserved) = tag([0x00])(input)?;
    // Bytes 5, ..., 16
    let (input, timings) = many_m_n(6, 6, parse_standard_timing_optional)(input)?;
    // Byte 17
    let (input, _padding) = tag([0x0a])(input)?;

    let timings = timings.into_iter().flatten().collect();

    Ok((
        input,
        DisplayDescriptor::StandardTimingIdentifiers(StandardTimingIdentifiers { timings }),
    ))
}

// Display color management data.
fn parse_color_management_data(input: Bytes) -> IResult<Bytes, DisplayDescriptor> {
    // Bytes 0, 1
    let (input, _dd) = tag([0x00, 0x00])(input)?;
    // Byte 2
    let (input, _reserved) = tag([0x00])(input)?;
    // Byte 3
    let (input, _descriptor_tag) = tag([0xf9])(input)?;
    // Byte 4
    let (input, _reserved) = tag([0x00])(input)?;
    // Byte 5
    let (input, version) = map(tag([0x03]), |x: &[u8]| x[0])(input)?;
    // Bytes 6, ..., 17
    let (input, red) = pair(le_u16, le_u16)(input)?;
    let (input, green) = pair(le_u16, le_u16)(input)?;
    let (input, blue) = pair(le_u16, le_u16)(input)?;

    Ok((
        input,
        DisplayDescriptor::ColorManagementData(ColorManagementData {
            version,
            red,
            green,
            blue,
        }),
    ))
}

// CVT 3-byte timing codes.
fn parse_cvt_lines_and_ratio(i: Bits) -> IResult<Bits, (u16, CVTAspectRatio)> {
    use CVTAspectRatio::*;
    // Combine 4 high and 8 low bits into a 12 bit number.
    let number12b = |high: u8, low: u8| u16::from(high) << 8 | u16::from(low);
    // Raw value is encoded as lines_per_field / 2 - 1.
    let decode = |value: u16| (value + 1) * 2;

    // Byte 6
    // Byte 7: Bits 7, 6, 5, 4
    // TODO: Assert byte 6 is not 0x00
    let (i, lines) = map(pair(take_bits(8usize), take_bits(4usize)), |(al_l, al_h)| {
        decode(number12b(al_h, al_l))
    })(i)?;

    // Byte 7: Bits 3, 2
    let (i, ratio) = map(take_bits(2usize), |ar| match ar {
        0b00 => AR4TO3,
        0b01 => AR16TO9,
        0b10 => AR16TO10,
        0b11 => AR15TO9,
        _ => unreachable!(),
    })(i)?;

    // Byte 7: Bits 1, 0
    let (i, _reserved) = tag_bits(0b00, 2usize)(i)?;

    Ok((i, (lines, ratio)))
}

fn parse_cvt_vertical_rates(i: Bits) -> IResult<Bits, (VerticalRate, Vec<(VerticalRate, BlankingType)>)> {
    use BlankingType::*;
    use VerticalRate::*;

    // Byte 8: Bit 7
    let (i, _reserved) = tag_bits(0b0, 1usize)(i)?;

    // Byte 8: Bits 6, 5
    let (i, preferred_rate) = map(take_bits(2usize), |pr| match pr {
        0b00 => RR50,
        0b01 => RR60,
        0b10 => RR75,
        0b11 => RR85,
        _ => unreachable!(),
    })(i)?;

    // Byte 8: Bits 4, 3, 2, 1, 0
    let (i, r1) = value_bitflag_set(i, (RR50, Standard))?;
    let (i, r2) = value_bitflag_set(i, (RR60, Standard))?;
    let (i, r3) = value_bitflag_set(i, (RR75, Standard))?;
    let (i, r4) = value_bitflag_set(i, (RR85, Standard))?;
    let (i, r5) = value_bitflag_set(i, (RR60, Reduced))?;

    let supported_rates = vec![r1, r2, r3, r4, r5];
    let supported_rates = supported_rates.into_iter().flatten().collect();

    Ok((i, (preferred_rate, supported_rates)))
}

fn parse_cvt_timing_code(input: Bytes) -> IResult<Bytes, CVTTimingCode> {
    // Bytes 6, 7
    let (input, (lines_per_field, aspect_ratio)) = bits(parse_cvt_lines_and_ratio)(input)?;
    // Byte 8
    let (input, (preferred_vertical_rate, supported_vertical_rates)) = bits(parse_cvt_vertical_rates)(input)?;

    Ok((
        input,
        CVTTimingCode {
            lines_per_field,
            aspect_ratio,
            preferred_vertical_rate,
            supported_vertical_rates,
        },
    ))
}

fn parse_cvt_timing_code_optional(input: Bytes) -> IResult<Bytes, Option<CVTTimingCode>> {
    alt((
        map(parse_cvt_timing_code, Some),
        map(tag([0x00, 0x00, 0x00]), |_| None),
    ))(input)
}

fn parse_cvt_timing_codes(input: Bytes) -> IResult<Bytes, DisplayDescriptor> {
    // Byte 0, 1
    let (input, _dd) = tag([0x00, 0x00])(input)?;
    // Byte 2
    let (input, _reserved) = tag([0x00])(input)?;
    // Byte 3
    let (input, _descriptor_tag) = tag([0xf8])(input)?;
    // Byte 4
    let (input, _reserved) = tag([0x00])(input)?;
    // Byte 5
    let (input, version) = map(tag([0x01]), |x: &[u8]| x[0])(input)?;
    // Bytes 6, ..., 17
    let (input, cvt_codes) = many_m_n(4, 4, parse_cvt_timing_code_optional)(input)?;

    let cvt_codes = cvt_codes.into_iter().flatten().collect();

    Ok((
        input,
        DisplayDescriptor::CVTTimingCodes(CVTTimingCodes { version, cvt_codes }),
    ))
}

// Established timings III.
fn parse_established_timing_iii_helper(i: Bits) -> IResult<Bits, Vec<EstablishedTimingIII>> {
    use EstablishedTimingIII::*;
    // Byte 6
    let (i, t01) = value_bitflag_set(i, H640V350F85)?;
    let (i, t02) = value_bitflag_set(i, H640V400F85)?;
    let (i, t03) = value_bitflag_set(i, H720V400F85)?;
    let (i, t04) = value_bitflag_set(i, H640V480F85)?;
    let (i, t05) = value_bitflag_set(i, H848V480F60)?;
    let (i, t06) = value_bitflag_set(i, H800V600F85)?;
    let (i, t07) = value_bitflag_set(i, H1024V768F85)?;
    let (i, t08) = value_bitflag_set(i, H1152V864F75)?;
    // Byte 7
    let (i, t11) = value_bitflag_set(i, H1280V768F60RB)?;
    let (i, t12) = value_bitflag_set(i, H1280V768F60)?;
    let (i, t13) = value_bitflag_set(i, H1280V768F75)?;
    let (i, t14) = value_bitflag_set(i, H1280V768F85)?;
    let (i, t15) = value_bitflag_set(i, H1280V960F60)?;
    let (i, t16) = value_bitflag_set(i, H1280V960F85)?;
    let (i, t17) = value_bitflag_set(i, H1280V1024F60)?;
    let (i, t18) = value_bitflag_set(i, H1280V1024F85)?;
    // Byte 8
    let (i, t21) = value_bitflag_set(i, H1360V768F60)?;
    let (i, t22) = value_bitflag_set(i, H1440V900F60RB)?;
    let (i, t23) = value_bitflag_set(i, H1440V900F60)?;
    let (i, t24) = value_bitflag_set(i, H1440V900F75)?;
    let (i, t25) = value_bitflag_set(i, H1440V900F85)?;
    let (i, t26) = value_bitflag_set(i, H1400V1050F60RB)?;
    let (i, t27) = value_bitflag_set(i, H1400V1050F60)?;
    let (i, t28) = value_bitflag_set(i, H1400V1050F75)?;
    // Byte 9
    let (i, t31) = value_bitflag_set(i, H1400V1050F85)?;
    let (i, t32) = value_bitflag_set(i, H1680V1050F60RB)?;
    let (i, t33) = value_bitflag_set(i, H1680V1050F60)?;
    let (i, t34) = value_bitflag_set(i, H1680V1050F75)?;
    let (i, t35) = value_bitflag_set(i, H1680V1050F85)?;
    let (i, t36) = value_bitflag_set(i, H1600V1200F60)?;
    let (i, t37) = value_bitflag_set(i, H1600V1200F65)?;
    let (i, t38) = value_bitflag_set(i, H1600V1200F70)?;
    // Byte 10
    let (i, t41) = value_bitflag_set(i, H1600V1200F75)?;
    let (i, t42) = value_bitflag_set(i, H1600V1200F85)?;
    let (i, t43) = value_bitflag_set(i, H1792V1344F60)?;
    let (i, t44) = value_bitflag_set(i, H1792V1344F75)?;
    let (i, t45) = value_bitflag_set(i, H1856V1392F60)?;
    let (i, t46) = value_bitflag_set(i, H1856V1392F75)?;
    let (i, t47) = value_bitflag_set(i, H1920V1200F60RB)?;
    let (i, t48) = value_bitflag_set(i, H1920V1200F60)?;
    // Byte 11
    let (i, t51) = value_bitflag_set(i, H1920V1200F75)?;
    let (i, t52) = value_bitflag_set(i, H1920V1200F85)?;
    let (i, t53) = value_bitflag_set(i, H1920V1440F60)?;
    let (i, t54) = value_bitflag_set(i, H1920V1440F75)?;
    // Reserved bits
    let (i, _reserved) = count(drop_bit, 4)(i)?;

    #[rustfmt::skip]
    let timings = vec![
        t01, t02, t03, t04, t05, t06, t07, t08,
        t11, t12, t13, t14, t15, t16, t17, t18,
        t21, t22, t23, t24, t25, t26, t27, t28,
        t31, t32, t33, t34, t35, t36, t37, t38,
        t41, t42, t43, t44, t45, t46, t47, t48,
        t51, t52, t53, t54,
    ];
    let timings = timings.into_iter().flatten().collect();

    Ok((i, timings))
}

fn parse_established_timings_iii(input: Bytes) -> IResult<Bytes, DisplayDescriptor> {
    // Bytes 0, 1
    let (input, _dd) = tag([0x00, 0x00])(input)?;
    // Byte 2
    let (input, _reserved) = tag([0x00])(input)?;
    // Byte 3
    let (input, _descriptor_tag) = tag([0xf7])(input)?;
    // Byte 4
    let (input, _reserved) = tag([0x00])(input)?;
    // Byte 5
    let (input, version) = map(tag([0x0a]), |x: &[u8]| x[0])(input)?;
    // Bytes 6, ..., 11
    let (input, timings) = bits(parse_established_timing_iii_helper)(input)?;
    // Bytes 12, ..., 17
    let (input, _padding) = tag([0x00, 0x00, 0x00, 0x00, 0x00, 0x00])(input)?;

    Ok((
        input,
        DisplayDescriptor::EstablishedTimingsIII(EstablishedTimingsIII { version, timings }),
    ))
}

// Reserved: currently undefined.
fn parse_reserved_descriptor(input: Bytes) -> IResult<Bytes, DisplayDescriptor> {
    let valid_tags = 0x11..=0xf6;

    // Bytes 0, 1
    let (input, _dd) = tag([0x00, 0x00])(input)?;
    // Byte 2
    let (input, _reserved) = tag([0x00])(input)?;
    // Byte 3
    let (input, descriptor_tag) = verify(le_u8, |x: &u8| valid_tags.contains(x))(input)?;
    // Byte 4
    let (input, _reserved) = tag([0x00])(input)?;
    // Bytes 5, ..., 17
    // TODO: Take or drop the reserved bytes?
    let (input, _bytes) = take(13usize)(input)?;

    Ok((
        input,
        DisplayDescriptor::ReservedDescriptor(ReservedDescriptor {
            tag: descriptor_tag,
            // bytes: bytes.try_into().unwrap_or([0x00; 13])
        }),
    ))
}

// Dummy descriptor.
fn parse_dummy_descriptor(input: Bytes) -> IResult<Bytes, DisplayDescriptor> {
    // Bytes 0, 1
    let (input, _dd) = tag([0x00, 0x00])(input)?;
    // Byte 2
    let (input, _reserved) = tag([0x00])(input)?;
    // Byte 3
    let (input, _descriptor_tag) = tag([0x10])(input)?;
    // Byte 4
    let (input, _reserved) = tag([0x00])(input)?;
    // Bytes 5, ..., 17
    // TODO: Be more or less strict here?
    // let (input, _) = take(13usize)(input)?;
    let (input, _) = tag([0x00; 13])(input)?;

    Ok((input, DisplayDescriptor::DummyDescriptor(DummyDescriptor {})))
}

// Manufacturer specified data.
fn parse_manufacturer_specified(input: Bytes) -> IResult<Bytes, DisplayDescriptor> {
    let valid_tags = 0x00..=0x0f;

    // Bytes 0, 1
    let (input, _dd) = tag([0x00, 0x00])(input)?;
    // Byte 2
    let (input, _reserved) = tag([0x00])(input)?;
    // Byte 3
    let (input, descriptor_tag) = verify(le_u8, |x: &u8| valid_tags.contains(x))(input)?;
    // Byte 4
    let (input, _reserved) = tag([0x00])(input)?;
    // Bytes 5, ..., 17
    let (input, bytes) = map(take(13usize), encode)(input)?;

    Ok((
        input,
        DisplayDescriptor::ManufacturerSpecified(ManufacturerSpecified {
            tag: descriptor_tag,
            bytes,
        }),
    ))
}

fn parse_display_descriptor(input: Bytes) -> IResult<Bytes, DisplayDescriptor> {
    let (input, descriptor) = alt((
        parse_product_serial_number,
        parse_alphanumeric_data_string,
        parse_range_limits,
        parse_product_name,
        parse_color_point_data,
        parse_standard_timing_identifiers,
        parse_color_management_data,
        parse_cvt_timing_codes,
        parse_established_timings_iii,
        parse_reserved_descriptor,
        parse_dummy_descriptor,
        parse_manufacturer_specified,
    ))(input)?;

    Ok((input, descriptor))
}

fn parse_18byte_descriptor(input: Bytes) -> IResult<Bytes, Either<DetailedTiming, DisplayDescriptor>> {
    alt((
        map(parse_detailed_timing, Either::Left),
        map(parse_display_descriptor, Either::Right),
    ))(input)
}

fn parse_18byte_descriptors(input: Bytes) -> IResult<Bytes, (DetailedTimings, DisplayDescriptors)> {
    use Either::{Left, Right};

    // Parse a mix of up to 4 DTDs or DDDs.
    let (input, things) = many_m_n(4, 4, parse_18byte_descriptor)(input)?;

    // Sort and collect found things.
    let detailed_timings = things
        .iter()
        .filter_map(|x| match x {
            Left(a) => Some(*a),
            Right(_) => None,
        })
        .collect();
    let display_descriptors = things
        .iter()
        .filter_map(|x| match x {
            Left(_) => None,
            Right(a) => Some(a.clone()),
        })
        .collect();

    Ok((
        input,
        (
            DetailedTimings(detailed_timings),
            DisplayDescriptors(display_descriptors),
        ),
    ))
}

impl Parse for (DetailedTimings, DisplayDescriptors) {
    fn parse(input: Bytes) -> IResult<Bytes, Self> {
        parse_18byte_descriptors(input)
    }
}

// SECTION 3.11 Extension Flag

fn parse_extension_count(input: Bytes) -> IResult<Bytes, u8> {
    le_u8(input)
}

fn parse_checksum(input: Bytes) -> IResult<Bytes, u8> {
    le_u8(input)
}

// All together

fn parse_edid_base_block(input: Bytes) -> IResult<Bytes, EDIDBaseBlock> {
    // 8 Bytes
    let (input, header) = Header::parse(input)?;

    // 10 Bytes
    let (input, product) = VendorProductIdentification::parse(input)?;

    // 2 Bytes
    let (input, version) = Version::parse(input)?;

    // 5 Bytes
    let (input, display_parameters) = DisplayParameters::parse(input)?;

    // 10 Bytes
    let (input, chromaticity) = ChromaticityCoordinates::parse(input)?;

    // 3 Bytes
    let (input, established_timings) = EstablishedTimings::parse(input)?;

    // 16 Bytes
    let (input, standard_timings) = StandardTimings::parse(input)?;

    // 4 times 18 Bytes
    let (input, (detailed_timings, display_descriptors)) =
        <(DetailedTimings, DisplayDescriptors)>::parse(input)?;

    // 1 Byte
    let (input, extension_count) = parse_extension_count(input)?;

    // 1 Byte
    let (input, checksum) = parse_checksum(input)?;

    Ok((
        input,
        EDIDBaseBlock {
            header,
            version,
            product,
            display_parameters,
            chromaticity,
            established_timings,
            standard_timings,
            detailed_timings,
            display_descriptors,
            extension_count,
            checksum,
        },
    ))
}

impl Parse for EDIDBaseBlock {
    /// Parse a base block.
    fn parse(input: Bytes) -> IResult<Bytes, Self> {
        parse_edid_base_block(input)
    }
}
